import { useCallback, useState } from 'react'
import PropTypes from 'prop-types'

const LoadableButton = ButtonComponent => {
  const LoadableButton = ({ onClick, disabled, ...props }) => {
    const [loading, setLoadingState] = useState(false)
    const HandleClick = useCallback(
      async (...args) => {
        try {
          setLoadingState(true)
          return await onClick(...args)
        } finally {
          setLoadingState(false)
        }
      },
      [onClick],
    )

    return <ButtonComponent {...props} onClick={HandleClick} disabled={disabled || loading} loading={loading} />
  }
  LoadableButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
  }

  LoadableButton.defaultProps = {
    disabled: false,
  }

  return LoadableButton
}

export default LoadableButton
