import { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Icon from '../Icon'
import LoadableButton from './LoadableButton'
import Preloader from '../../Icons/preloader'

import { Button } from './styles'
export { Button, LoadableButton }

const LoaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`

const BaseButton = ({ className, loading, children, type, ...props }, ref) => (
  <Button {...props} className={`${className} relative`} type={type} ref={ref}>
    {loading && (
      <LoaderContainer className="flex justify-center items-center pointer-events-none">
        <Icon size="25" className="color-lightGold" icon={Preloader} />
      </LoaderContainer>
    )}
    {children}
  </Button>
)

BaseButton.propTypes = {
  loading: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  type: PropTypes.string,
}

BaseButton.defaultProps = {
  className: '',
  classNameChildren: '',
  type: 'button',
  loading: false,
  children: null,
}

export default forwardRef(BaseButton)
