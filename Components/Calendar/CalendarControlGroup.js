import PropTypes from 'prop-types'
import { NavigationButton, NavigationButtonsContainer, NavigationLabel } from './styles'
import Icon from '../Icon'
import DoubleLeftChevron from './icons/doubleLeftChevron'
import DoubleRightChevron from './icons/doubleRightChevron'
import LeftChevron from './icons/leftChevron'
import RightChevron from './icons/rightChevron'

export const SINGLE = 'singe'
export const DOUBLE = 'double'
export { NavigationButton, NavigationLabel }

const CalendarControlGroup = ({ disabled, calendarLabel, onNavigation, onChangeView }) => {
  return (
    <NavigationButtonsContainer className="whitespace-nowrap">
      <>
        <NavigationButton type="button" onClick={onNavigation(DOUBLE, false)}>
          <Icon icon={DoubleLeftChevron} />
        </NavigationButton>
        <NavigationButton type="button" onClick={onNavigation(SINGLE, false)}>
          <Icon icon={LeftChevron} />
        </NavigationButton>
      </>
      <NavigationLabel type="button" disabled={disabled} onClick={onChangeView}>
        {calendarLabel}
      </NavigationLabel>
      <>
        <NavigationButton type="button" onClick={onNavigation(SINGLE, true)}>
          <Icon icon={RightChevron} />
        </NavigationButton>
        <NavigationButton type="button" onClick={onNavigation(DOUBLE, true)}>
          <Icon icon={DoubleRightChevron} />
        </NavigationButton>
      </>
    </NavigationButtonsContainer>
  )
}

CalendarControlGroup.propTypes = {
  disabled: PropTypes.bool,
  calendarLabel: PropTypes.string.isRequired,
  onNavigation: PropTypes.func.isRequired,
  onChangeView: PropTypes.func.isRequired,
}

CalendarControlGroup.defaultProps = {
  disabled: false,
}

export default CalendarControlGroup
