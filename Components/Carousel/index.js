import { useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import Icon from '../Icon'
import { arrowleft } from './icons/arrowleft'
import { arrowright } from './icons/arrowright'
import { ArrowsContainer, SlidesContainer } from './styles'
import debounce from 'lodash/debounce'

const Carousel = ({ children, arrowLeft: ArrowLeft, arrowRight: ArrowRight, value, onInput }) => {
  const [scrollPosition, updateScrollPosition] = useState(0)
  const [Comp, setComp] = useState('div')
  const refValue = useRef(value)
  const carouselContainer = useRef({})
  const slidesContainer = useRef({})
  const transitionState = useMemo(() => ({ transform: `translateX(${-scrollPosition}px)` }), [scrollPosition])
  const { children: slides = {} } = slidesContainer.current

  const updateScroll = useMemo(() => {
    const handler = () => {
      if (carouselContainer.current !== null) {
        const { current: value } = refValue
        const { clientWidth: containerWidth } = carouselContainer.current
        const {
          children: {
            [value]: { clientWidth, offsetLeft },
          },
        } = slidesContainer.current
        const offset = offsetLeft + clientWidth - containerWidth

        updateScrollPosition(offset < 0 ? (offsetLeft > containerWidth ? offsetLeft - containerWidth : 0) : offset)
      }
    }

    return debounce(
      Comp === SlidesContainer
        ? handler
        : new Proxy(handler, {
            apply(target, thisArg, argArray) {
              target.apply(thisArg, argArray)
              setTimeout(() => {
                setComp(SlidesContainer)
              }, 10)
            },
          }),
      25,
    )
  }, [Comp])

  useLayoutEffect(() => {
    refValue.current = value
    updateScroll()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  useEffect(() => {
    const observer = new ResizeObserver(updateScroll)
    observer.observe(carouselContainer.current)
    return () => {
      observer.disconnect()
    }
  }, [updateScroll])

  const handleChangeTab = useCallback(index => () => onInput(index), [onInput])

  const handleHorizontalScroll = useCallback(
    e => {
      e.stopPropagation()
      const { deltaY } = e
      if ((value > 0 && deltaY < 0) || (value < slides.length - 1 && deltaY > 0)) {
        handleChangeTab(deltaY > 0 ? value + 1 : value - 1)()
      }
    },
    [value, slides.length, handleChangeTab],
  )

  return (
    <div className="w-full h-full flex-auto flex overflow-hidden">
      <ArrowsContainer onClick={handleChangeTab(value - 1)} disabled={slides[value - 1] === undefined}>
        <ArrowLeft />
      </ArrowsContainer>
      <ArrowsContainer onClick={handleChangeTab(value + 1)} disabled={slides[value + 1] === undefined}>
        <ArrowRight />
      </ArrowsContainer>
      <div className="w-full h-full flex-auto flex overflow-hidden" ref={carouselContainer}>
        <Comp className="flex flex-auto" style={transitionState} ref={slidesContainer} onWheel={handleHorizontalScroll}>
          {children}
        </Comp>
      </div>
    </div>
  )
}

Carousel.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  arrowLeft: PropTypes.func,
  arrowRight: PropTypes.func,
  value: PropTypes.number.isRequired,
  onInput: PropTypes.func.isRequired,
}

Carousel.defaultProps = {
  arrowLeft: () => <Icon icon={arrowleft} className="px-1.5" />,
  arrowRight: () => <Icon icon={arrowright} className="px-1.5" />,
  children: null,
}

export default Carousel
