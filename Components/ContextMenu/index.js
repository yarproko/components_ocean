import { useLayoutEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import useAutoCloseFrame from '../../Utils/Hooks/useAutoCloseFrame'

const ContextMenuContainer = styled.div`
  z-index: 1000;
`
const defaultState = { style: { top: -1000, left: -1000 } }
const ContextMenu = ({ children, onClose, target, width: containerWidth, className }) => {
  const [portalState, setPortalStyles] = useState(defaultState)
  const eventInterceptor = useAutoCloseFrame(onClose)
  const elementRef = useRef()
  const containerRef = useRef()
  useLayoutEffect(() => {
    const interval = setInterval(() => {
      const { x, bottom, width } = (target || elementRef.current.parentNode).getBoundingClientRect()
      setPortalStyles(state => {
        if (x !== state.x || bottom !== state.bottom || width !== state.width) {
          const nextWidth = containerWidth || width
          return {
            x,
            bottom,
            width: nextWidth,
            style: {
              left: `${x}px`,
              top: `${bottom}px`,
              width: `${nextWidth}px`,
            },
          }
        }
        return state
      })
    }, 5)
    return () => clearInterval(interval)
  }, [containerWidth, target])

  useLayoutEffect(() => {
    const { bottom, right, width } = containerRef.current.children[0].getBoundingClientRect()
    const { clientWidth, clientHeight } = document.body
    // проверяем, не вылез контейнер меню за пределы документа, если вылез, рендерим слева на право
    if (clientWidth < right) {
      setPortalStyles(s => {
        if (s.right === right) {
          return s
        }
        const { style, ...state } = s
        return {
          ...state,
          right,
          style: {
            ...style,
            right: `${width - state.width > 0 ? width - state.width : 0}px`, // e
          },
        }
      })
    }
    // проверяем, не вылез контейнер меню за пределы документа, если вылез, рендерим снизу вверх
    if (clientHeight < bottom) {
      setPortalStyles(s => {
        if (s.bottom === bottom) {
          return s
        }
        const { style, ...state } = s
        return {
          ...state,
          bottom,
          style: {
            ...style,
            bottom: `${0}px`,
          },
        }
      })
    }
  }, [portalState])

  return (
    <div ref={elementRef}>
      {ReactDOM.createPortal(
        <ContextMenuContainer
          ref={containerRef}
          className={`${className} fixed`}
          style={portalState.style}
          onMouseDown={eventInterceptor}
        >
          {children}
        </ContextMenuContainer>,
        document.body,
      )}
    </div>
  )
}

ContextMenu.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onClose: PropTypes.func.isRequired,
  target: PropTypes.instanceOf(Element),
  width: PropTypes.number,
}

ContextMenu.defaultProps = {
  className: '',
  children: null,
  target: undefined,
  width: undefined,
}

export default ContextMenu
