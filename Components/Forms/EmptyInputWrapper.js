import PropTypes from 'prop-types'
const EmptyInputWrapper = ({ children }) => children

EmptyInputWrapper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
}

export default EmptyInputWrapper
