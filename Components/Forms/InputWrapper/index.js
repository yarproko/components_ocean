import React from 'react'
import PropTypes from 'prop-types'
import { InputContainer, InputErrorContainer, InputLabel, InputLabelStart, InputWrapperContainer } from './styles'

export { InputWrapperContainer, InputLabel, InputContainer, InputErrorContainer, InputLabelStart }

const DefaultInputWrapper = React.forwardRef(
  ({ className, style, id, label, validationErrors, children, hasError, isRequired }, ref) => {
    return (
      <InputWrapperContainer
        className={`${className} flex flex-col flex-auto h-min`}
        style={style}
        ref={ref}
        hasError={hasError}
      >
        <InputLabel htmlFor={id}>
          {label} {isRequired && <InputLabelStart>*</InputLabelStart>}
        </InputLabel>
        <InputContainer className="flex flex-col flex-auto relative w-full">
          {children}
          {hasError && <InputErrorContainer>{validationErrors[0]}</InputErrorContainer>}
        </InputContainer>
      </InputWrapperContainer>
    )
  },
)

DefaultInputWrapper.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  label: PropTypes.string,
  validationErrors: PropTypes.array,
  className: PropTypes.string,
  style: PropTypes.object,
  hasError: PropTypes.bool,
  isRequired: PropTypes.bool,
}

DefaultInputWrapper.defaultProps = {
  className: '',
  label: '',
  validationErrors: [],
  style: undefined,
  hasError: false,
  isRequired: false,
}

export default DefaultInputWrapper
