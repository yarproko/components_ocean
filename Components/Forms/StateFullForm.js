import { useState } from 'react'
import PropTypes from 'prop-types'
import Form from './Form'

const StateFullForm = ({ initPayload, ...props }) => {
  const [formState, setState] = useState(initPayload)

  return <Form value={formState} onInput={setState} {...props} />
}

StateFullForm.propTypes = {
  initPayload: PropTypes.object,
}

StateFullForm.defaultProps = {
  initPayload: {},
}

export default StateFullForm
