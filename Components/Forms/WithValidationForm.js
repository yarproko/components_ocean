import WithValidationHoc from '../../Logic/Validator'
import Form from './Form'

const WithValidationForm = WithValidationHoc(Form)

export default WithValidationForm
