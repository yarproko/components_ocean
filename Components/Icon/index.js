import { useMemo } from 'react'
import PropTypes from 'prop-types'
import { IconContainer } from './styles'

export { IconContainer }

const renderIcon = children =>
  children.map((child, index) => {
    const { name: Icon, attribs, children = [] } = child
    return (
      <Icon key={index} {...attribs}>
        {renderIcon(children)}
      </Icon>
    )
  })

const Icon = ({
  children: ch,
  size,
  width = size,
  height = size,
  title,
  className,
  style,
  // eslint-disable-next-line react/prop-types
  icon: { children, viewBox, attribs },
  ...props
}) => (
  <IconContainer {...props} className={`${className} justify-center items-center flex`} style={style}>
    <svg fill="currentColor" height={height} width={width} viewBox={viewBox} {...attribs}>
      {title && <title>{title}</title>}
      {useMemo(() => renderIcon(children), [children])}
    </svg>
    {ch}
  </IconContainer>
)

Icon.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  title: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  // eslint-disable-next-line react/require-default-props
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  // eslint-disable-next-line react/require-default-props
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  icon: PropTypes.shape({
    children: PropTypes.array,
    viewBox: PropTypes.string,
  }).isRequired,
}
Icon.defaultProps = {
  size: 16,
  className: '',
  title: '',
  children: null,
  style: undefined,
}
export default Icon
