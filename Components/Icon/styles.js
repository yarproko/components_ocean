import styled from 'styled-components'

export const IconContainer = styled.div`
  transition-property: color, transform;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 250ms;
`
