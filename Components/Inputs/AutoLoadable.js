import memoize from 'lodash/memoize'
import { forwardRef, useEffect } from 'react'
import PropTypes from 'prop-types'

const AutoLoadable = memoize(Component => {
  const AutoLoadable = forwardRef((props, ref) => {
    const { remoteMethod } = props
    useEffect(remoteMethod, [remoteMethod])
    return <Component ref={ref} {...props} />
  })

  AutoLoadable.propTypes = {
    remoteMethod: PropTypes.func.isRequired,
  }

  AutoLoadable.defaultProps = {
    value: undefined,
  }

  return AutoLoadable
})

export default AutoLoadable
