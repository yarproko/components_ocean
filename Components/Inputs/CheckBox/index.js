import PropTypes from 'prop-types'
import { Box, BoxContainer, CheckBoxContainer, CheckBoxLabel } from './styles'
import { useChecked, useReverseChecked } from './logic'

export { BoxContainer, Box, CheckBoxLabel, CheckBoxContainer }

function preventDoubleClick(e) {
  e.preventDefault()
  e.stopPropagation()
}

const CheckBox = props => {
  const { disabled, text, className, style, useCheckedLogic, id } = props

  const { checked, updateValue } = useCheckedLogic(props)

  return (
    <CheckBoxContainer
      id={id}
      className={`${className} flex items-center`}
      style={style}
      disabled={disabled}
      type="button"
      onMouseDown={updateValue}
      onDoubleClick={preventDoubleClick}
      name={text}
    >
      <BoxContainer>
        <Box checked={checked} />
      </BoxContainer>
      {text !== undefined && <CheckBoxLabel>{text}</CheckBoxLabel>}
    </CheckBoxContainer>
  )
}

CheckBox.propTypes = {
  value: PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.object]),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  returnObjects: PropTypes.bool,
  disabled: PropTypes.bool,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  checkBoxValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  valueKey: PropTypes.oneOfType([PropTypes.string, PropTypes.symbol, PropTypes.number]),
  onInput: PropTypes.func.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  useCheckedLogic: PropTypes.func,
}

CheckBox.defaultProps = {
  value: undefined,
  checkBoxValue: undefined,
  style: undefined,
  text: '',
  returnObjects: false,
  disabled: false,
  valueKey: 'ID',
  onBlur: () => null,
  onFocus: () => null,
  className: '',
  useCheckedLogic: useChecked,
}

export const ReverseCheckBox = props => <CheckBox {...props} useCheckedLogic={useReverseChecked} />

export default CheckBox
