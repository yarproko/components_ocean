import { useCallback, useMemo } from 'react'

export const useChecked = ({ value, returnObjects, checkBoxValue, valueKey, id, onBlur, onFocus, onInput }) => {
  const normalizeCheckBoxVal = useMemo(
    () => (checkBoxValue !== null && typeof checkBoxValue === 'object' ? checkBoxValue[valueKey] : checkBoxValue),
    [checkBoxValue, valueKey],
  )

  const checked = useMemo(
    () =>
      checkBoxValue && Array.isArray(value)
        ? value.some(val => (returnObjects ? val[valueKey] : val) === normalizeCheckBoxVal)
        : !!value,
    [checkBoxValue, value, valueKey, normalizeCheckBoxVal, returnObjects],
  )

  const updateValue = useCallback(() => {
    onFocus()
    if (checkBoxValue) {
      onInput(
        checked
          ? value.filter(val => (typeof val === 'object' ? val[valueKey] : val) !== normalizeCheckBoxVal)
          : [
              ...(value || []),
              returnObjects
                ? typeof checkBoxValue === 'object'
                  ? checkBoxValue
                  : { [valueKey]: normalizeCheckBoxVal }
                : normalizeCheckBoxVal,
            ],
        id,
      )
    } else {
      onInput(!value, id)
    }
    onBlur()
  }, [onFocus, onInput, checkBoxValue, normalizeCheckBoxVal, value, checked, valueKey, returnObjects, id, onBlur])

  return { normalizeCheckBoxVal, checked, updateValue }
}

export const useReverseChecked = ({ value, returnObjects, checkBoxValue, valueKey, id, onBlur, onFocus, onInput }) => {
  const normalizeCheckBoxVal = useMemo(
    () => (checkBoxValue !== null && typeof checkBoxValue === 'object' ? checkBoxValue[valueKey] : checkBoxValue),
    [checkBoxValue, valueKey],
  )

  const checked = useMemo(
    () =>
      checkBoxValue && Array.isArray(value)
        ? value.every(val => (returnObjects ? val[valueKey] : val) !== normalizeCheckBoxVal)
        : !value,
    [checkBoxValue, value, valueKey, normalizeCheckBoxVal, returnObjects],
  )

  const updateValue = useCallback(() => {
    onFocus()
    if (checkBoxValue) {
      onInput(
        !checked
          ? value.filter(val => (typeof val === 'object' ? val[valueKey] : val) !== normalizeCheckBoxVal)
          : [
              ...(value || []),
              returnObjects
                ? typeof checkBoxValue === 'object'
                  ? checkBoxValue
                  : { [valueKey]: normalizeCheckBoxVal }
                : normalizeCheckBoxVal,
            ],
        id,
      )
    } else {
      onInput(!value, id)
    }
    onBlur()
  }, [onFocus, onInput, checkBoxValue, normalizeCheckBoxVal, value, checked, valueKey, returnObjects, id, onBlur])

  return { normalizeCheckBoxVal, checked, updateValue }
}
