import styled from 'styled-components'

export const CheckBoxContainer = styled.button``

export const BoxContainer = styled.div`
  width: 16px;
  height: 16px;
  border: 1px solid var(--form-elements-border-color, #bdbdbd);
  position: relative;
  flex: 0 0 auto;
  ${CheckBoxContainer}:disabled {
    --border-color-input: var(--form-elements-disabled-border-color, #dddddd);
    color: var(--input-disabled-color, #bdbdbd);
  }
`

export const Box = styled.div`
  transition-property: background-color, border-color, transform;
  transition-timing-function: linear;
  transition-duration: 150ms;
  background-color: var(--form-elements-border-color, #bdbdbd);
  transform: ${props => (props.checked ? 'scale(0.75)' : 'scale(0)')};
  position: absolute;
  width: 100%;
  height: 100%;
  margin: auto;
  ${CheckBoxContainer}:disabled {
    background-color: var(--form-elements-border-color, #bdbdbd) !important;
  }
`

export const CheckBoxLabel = styled.div`
  margin-left: 16px;
`
