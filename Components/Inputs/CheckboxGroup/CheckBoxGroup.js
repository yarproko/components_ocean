import PropTypes from 'prop-types'
import CheckBox from '../CheckBox'
import { CheckBoxContainer } from './styles'
import { renderKey } from './constants'

export { CheckBoxContainer }

const CheckBoxGroup = ({ className, style, disabled, options, labelKey, checkBoxComponent: Comp, ...props }) => {
  return (
    <CheckBoxContainer className={`${className} flex flex-col`} style={style}>
      {options.map(item => {
        const { [labelKey]: label, [renderKey]: key } = item
        return <Comp {...props} className="mb-4" checkBoxValue={item} label={label} disabled={disabled} key={key} />
      })}
    </CheckBoxContainer>
  )
}

CheckBoxGroup.propTypes = {
  options: PropTypes.array,
  labelKey: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  style: PropTypes.object,
  checkBoxComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
}

CheckBoxGroup.defaultProps = {
  options: [],
  labelKey: 'label',
  disabled: false,
  style: undefined,
  className: '',
  checkBoxComponent: CheckBox,
}

export default CheckBoxGroup
