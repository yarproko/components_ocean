import React, { useCallback, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { HeaderContainer, HeaderInput } from './styles'
import CloseIcon from '../../../Icons/close'
import Search from '../../../Icons/search'
import Icon from '../../Icon'
import CheckBox from '../../Inputs/CheckBox'

const CheckBoxHeader = React.forwardRef(
  (
    {
      options,
      value,
      disabled,
      returnObjects,
      valueKey,
      onInput,
      id,
      checkBoxComponent: Comp,
      title,
      searchQuery,
      setSearchQuery,
      placeholder,
    },
    ref,
  ) => {
    const [edited, setEdited] = useState(false)

    const handleEscape = useCallback(({ key }) => {
      if (key === 'Escape') {
        setEdited(false)
      }
    }, [])

    const dropSearch = useCallback(() => setSearchQuery(''), [setSearchQuery])

    const openSearchField = useCallback(() => setEdited(true), [])

    const optionComparator = useMemo(
      () => (returnObjects ? opt => val => val[valueKey] === opt[valueKey] : opt => val => val === opt[valueKey]),
      [valueKey, returnObjects],
    )

    const isSelected = useMemo(
      () =>
        options.length > 0 &&
        options.every(opt => {
          const comparator = optionComparator(opt)
          return value.some(comparator)
        }),
      [optionComparator, options, value],
    )

    const handleInput = useCallback(
      nextValue => {
        const nArray = Array.from(value)
        if (nextValue) {
          options.forEach(opt => {
            const comparator = optionComparator(opt)
            if (!nArray.some(comparator)) {
              nArray.push(returnObjects ? opt : opt[valueKey])
            }
          })
        } else {
          options.forEach(opt => {
            const comparator = optionComparator(opt)
            const i = nArray.findIndex(comparator)
            if (i >= 0) {
              nArray.splice(i, 1)
            }
          })
        }
        onInput(nArray, id)
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [value, onInput, id, options, returnObjects, valueKey],
    )

    return (
      <HeaderContainer className="flex items-center w-full">
        <Comp id="selectAll" value={isSelected} disabled={disabled} onInput={handleInput} />
        {!edited ? (
          <div className="pl-3">{title}</div>
        ) : (
          <HeaderInput
            ref={ref}
            type="text"
            autoFocus
            placeholder={placeholder}
            value={searchQuery}
            onInput={setSearchQuery}
            onKeyup={handleEscape}
            className="ml-2.5 p-0"
            id={1}
          />
        )}
        {searchQuery ? (
          <Icon icon={CloseIcon} size="12" className={'ml-auto cursor'} onClick={dropSearch} />
        ) : (
          <Icon
            icon={Search}
            className={`ml-auto ${edited ? 'active' : ''} cursor`}
            size="14"
            onClick={openSearchField}
          />
        )}
      </HeaderContainer>
    )
  },
)

CheckBoxHeader.propTypes = {
  valueKey: PropTypes.oneOfType([PropTypes.string, PropTypes.symbol, PropTypes.number]),
  onInput: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setSearchQuery: PropTypes.func.isRequired,
  options: PropTypes.array,
  value: PropTypes.array,
  disabled: PropTypes.bool,
  returnObjects: PropTypes.bool,
  checkBoxComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  title: PropTypes.string,
  searchQuery: PropTypes.string,
  placeholder: PropTypes.string,
}

CheckBoxHeader.defaultProps = {
  valueKey: 'ID',
  disabled: false,
  returnObjects: false,
  title: '',
  searchQuery: '',
  value: [],
  options: [],
  checkBoxComponent: CheckBox,
  placeholder: 'Please enter query',
}

export default CheckBoxHeader
