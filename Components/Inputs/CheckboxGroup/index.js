import React, { useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import CheckBoxGroup, { CheckBoxContainer } from './CheckBoxGroup'
import CheckBoxHeader from './CheckBoxHeader'
import { renderKey } from './constants'
import uniqueId from 'lodash/uniqueId'
import { GroupContainer } from './styles'

export { CheckBoxContainer }

const searchLabel = Symbol('searchLabel') // аттрибут объекта для приведения лабеля к нижнему регистру
const qPosition = Symbol('qPosition') // аттрибут объекта для расчета совпадения строки поиска с лабелем объекта

const CheckBoxGroupInput = React.forwardRef(
  ({ headerComponent: HeaderComp, groupComponent: GroupComp, options, className, style, ...props }, ref) => {
    const { labelKey } = props
    const [searchQuery, setSearchQuery] = useState('')

    // приводим к нижнему регистру все лабеля объектов
    const castOptionsToLowerCase = useMemo(
      () => options.map(o => ({ ...o, [searchLabel]: String(o[labelKey]).toLowerCase(), [renderKey]: uniqueId() })),
      [options, labelKey],
    )

    const sortOptions = useMemo(() => {
      // приводим строку поиска к нижнему регистру
      const q = searchQuery.toLowerCase()
      return (
        castOptionsToLowerCase
          .reduce((acc, item) => {
            // проверяем есть ли  совпадения лабеля со строкой поиска, сохраняем себе объект с вычесленной позицией совпадения
            const itemQpos = item[searchLabel].indexOf(q)
            if (itemQpos >= 0) {
              acc.push({ ...item, [qPosition]: itemQpos })
            }
            return acc
          }, [])
          // сортируем вверх самые релевантные значения
          .sort(({ [qPosition]: itemQpos }, { [qPosition]: S_itemQpos }) => (itemQpos > S_itemQpos ? 1 : -1))
      )
    }, [castOptionsToLowerCase, searchQuery])

    return (
      <div className={`${className} flex flex-col`} style={style}>
        <GroupContainer>
          <HeaderComp
            {...props}
            ref={ref}
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            options={sortOptions}
          />
          <GroupComp options={sortOptions} {...props} />
        </GroupContainer>
      </div>
    )
  },
)

CheckBoxGroupInput.propTypes = {
  groupComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  headerComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  labelKey: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  value: PropTypes.array,
  options: PropTypes.array,
  returnObjects: PropTypes.bool,
  disabled: PropTypes.bool,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
}

CheckBoxGroupInput.defaultProps = {
  headerComponent: CheckBoxHeader,
  groupComponent: CheckBoxGroup,
  className: '',
  options: [],
  value: [],
  returnObjects: false,
  disabled: false,
  onBlur: () => null,
  onFocus: () => null,
  style: undefined,
}

export default CheckBoxGroupInput
