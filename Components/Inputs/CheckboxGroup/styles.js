import styled from 'styled-components'
import Input from '../Input'
import ScrollBar from 'react-perfect-scrollbar'

export const CheckBoxContainer = styled(ScrollBar)`
  height: var(--height-checkboxGroup-container, 200px);
  padding: 0 0.8rem 0.5rem 0.8rem;
`

export const HeaderInput = styled(Input)`
  --padding-input: 0px;
  --form--elements_height: auto;
  border-width: 0 !important;
  background: var(--background-checkboxGroup-header, #545454);
  color: var(--color-checkboxGroup-header, #ffffff);
`

export const HeaderContainer = styled.div`
  border-bottom: 1px solid var(--color-border-checkboxGroup, #bdbdbd);
  margin-bottom: 0.5rem;
  background: var(--background-checkboxGroup-header, #545454);
  border-radius: 4px;
  padding: 0.5rem 0.8rem;
  color: var(--color-checkboxGroup-header, #ffffff);
`

export const GroupContainer = styled.div`
  position: relative;
  padding-bottom: 0.5rem;
  &::after {
    position: absolute;
    bottom: 0;
    content: '';
    border-bottom: 1px solid var(--color-border-checkboxGroup, #bdbdbd);
    left: 0;
    right: 0;
  }
`
