import PropTypes from 'prop-types'
import ContextMenu from '../../ContextMenu'
import { ModalContainer } from './styles'

const DropDownComponent = ({ isOpen, children, ...props }) =>
  isOpen && (
    <ContextMenu {...props}>
      <ModalContainer>{children}</ModalContainer>
    </ContextMenu>
  )

DropDownComponent.propTypes = {
  isOpen: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
}

DropDownComponent.defaultProps = {
  isOpen: false,
  children: null,
}

export default DropDownComponent
