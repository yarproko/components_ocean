import { useCallback, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { Container } from '../styles'
import StyledHtmlInput from '../StyledHtmlInput'
import { DatePickerCalendar } from './styles'
import dayjs from 'dayjs'
import InputMask from '../InputMask'
import DropDownComponent from './DropDownComponent'

const SEPARATOR = '-'

const DatePicker = ({
  disabled,
  DropDownComponent,
  className,
  leftSlot,
  dateFormat,
  onFocus,
  onBlur,
  CalendarComponent,
  value,
  id,
  placeholder,
  onInput,
  range,
  ...props
}) => {
  const [manualInput, setManualInput] = useState('')
  const [isOpen, setOpenState] = useState(false)

  const renderValue = useMemo(() => (Array.isArray(value) ? value.join(SEPARATOR) : value), [value])

  const handleInput = useCallback(
    v => {
      onInput(v, id)
    },
    [id, onInput],
  )

  const handleCalendarInput = useCallback(
    v => {
      setOpenState(false)
      handleInput(v)
      onBlur()
    },
    [handleInput, onBlur],
  )

  const closeDropDown = useCallback(() => {
    setOpenState(false)

    setManualInput(manualInput => {
      if (manualInput !== undefined && manualInput !== renderValue) {
        let val
        if (range) {
          const separatedValues = manualInput.split(SEPARATOR)
          if (separatedValues.every(date => dayjs(date, dateFormat, true).isValid())) {
            val = separatedValues
          }
        } else if (dayjs(manualInput, dateFormat, true).isValid()) {
          val = manualInput
        }
        handleInput(val)
      }
      return manualInput
    })
    onBlur()
  }, [dateFormat, handleInput, onBlur, range, renderValue])

  const onBlurClose = useCallback(() => {
    if (!isOpen) {
      closeDropDown()
    }
  }, [closeDropDown, isOpen])

  const openDropDown = useCallback(() => {
    if (!disabled) {
      setOpenState(true)
      onFocus()
    }
  }, [disabled, onFocus])

  useEffect(() => {
    setManualInput(renderValue)
  }, [renderValue])

  const inputMask = useMemo(() => {
    const mask = dateFormat.replace(/[A-Z]/gi, '9')
    return range ? `${mask}${SEPARATOR}${mask}` : mask
  }, [dateFormat, range])

  return (
    <Container className={`${className} flex`} disabled={disabled}>
      {leftSlot}
      <div className="flex items-center relative w-full">
        <InputMask
          id={id}
          mask={inputMask}
          value={manualInput}
          onInput={setManualInput}
          placeholder={placeholder}
          onFocus={openDropDown}
          disabled={disabled}
          inputComponent={StyledHtmlInput}
          onBlur={onBlurClose}
        />
      </div>
      <DropDownComponent onClose={closeDropDown} isOpen={isOpen}>
        <CalendarComponent
          value={value}
          onInput={handleCalendarInput}
          dateFormat={dateFormat}
          range={range}
          {...props}
        />
      </DropDownComponent>
    </Container>
  )
}

DatePicker.propTypes = {
  disabled: PropTypes.bool,
  className: PropTypes.string,
  leftSlot: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  dateFormat: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  placeholder: PropTypes.string,
  onInput: PropTypes.func.isRequired,
  range: PropTypes.bool,
  DropDownComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  CalendarComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
}

DatePicker.defaultProps = {
  disabled: false,
  className: '',
  leftSlot: null,
  placeholder: '',
  range: false,
  value: undefined,
  DropDownComponent: DropDownComponent,
  CalendarComponent: DatePickerCalendar,
  dateFormat: 'DD.MM.YYYY',
  onBlur: () => null,
  onFocus: () => null,
}

export default DatePicker
