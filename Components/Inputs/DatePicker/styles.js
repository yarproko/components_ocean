import styled from 'styled-components'
import Calendar from '../../Calendar/StateFullCalendar'

export const DatePickerCalendar = styled(Calendar)`
  padding: 0;
  margin-top: 15px;
  border-radius: 2px;
`

export const ModalContainer = styled.div`
  width: 392px;
  background: white;
  box-shadow: 0px 4px 11px rgba(0, 0, 0, 0.1);
  padding: 24px 32px;
`
