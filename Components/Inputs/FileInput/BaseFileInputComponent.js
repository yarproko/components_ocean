import PropTypes from 'prop-types'
import Icon from '../../Icon'
import ClipIcon from './Icons/ClipIcon'
import { FileInputContainer, ModalSubmitButton, RejectedFilesModalWindow } from './style'
import { FILE_INPUT_ERROR_EXTENSION, FILE_INPUT_ERROR_SIZE } from './constants'
import Option from './Option'
import { useCallback } from 'react'

const WrongFileExtension = ({ fileName, payload: { mimeTypes, positiveLock } }) =>
  `${fileName} have wrong file extension. ${
    positiveLock ? 'Allowed' : 'Forbidden'
  } file extensions are: ${mimeTypes.join(', ')}`
const ExceedsFileSizeExtension = ({ fileName, payload }) =>
  `${fileName} exceeds allowed file size. Allowed file size is ${payload}`

const exceptionsMessagesMap = {
  [FILE_INPUT_ERROR_SIZE]: ExceedsFileSizeExtension,
  [FILE_INPUT_ERROR_EXTENSION]: WrongFileExtension,
}

const BaseFileInputComponent = ({ openFileInput, onDelete, onReUpload, rejectedFiles, setRejectedFiles, value }) => {
  const closeModalWindow = useCallback(() => setRejectedFiles([]), [setRejectedFiles])
  return (
    <>
      <FileInputContainer onClick={openFileInput} type="button" className="items-center">
        <Icon icon={ClipIcon} className="mr-2" size={18} />
        {value.length > 0 ? (
          value.map((v, index) => (
            <Option key={index} value={v} onDelete={onDelete(index)} onReUpload={onReUpload(index)} />
          ))
        ) : (
          <span>Or Drop here</span>
        )}
      </FileInputContainer>
      <RejectedFilesModalWindow open={rejectedFiles.length > 0} className="text-center" onClose={closeModalWindow}>
        <h2>Those files can't be uploaded</h2>
        <div className="mb-8">
          {rejectedFiles.map((error, i) => (
            <div key={i}>{exceptionsMessagesMap[error.message](error)}</div>
          ))}
        </div>
        <ModalSubmitButton className="mx-auto mt-auto mb-2" onClick={closeModalWindow} type="button">
          Chose other message
        </ModalSubmitButton>
      </RejectedFilesModalWindow>
    </>
  )
}

BaseFileInputComponent.propTypes = {
  openFileInput: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onReUpload: PropTypes.func.isRequired,
  rejectedFiles: PropTypes.array,
  setRejectedFiles: PropTypes.func.isRequired,
  value: PropTypes.array,
}

BaseFileInputComponent.defaultProps = {
  value: [],
  rejectedFiles: [],
}

export default BaseFileInputComponent
