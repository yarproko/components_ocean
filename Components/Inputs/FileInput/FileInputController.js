import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'

import uniqueId from 'lodash/uniqueId'
import Pipe from '../../../Utils/FunctionCall/pipe'
import PureDeleteItems from '../../../Utils/Arrays/PureDeleteItems'
import { FILE_INPUT_ERROR_EXTENSION, FILE_INPUT_ERROR_SIZE } from './constants'

class FileValidation extends Error {
  constructor({ message, fileName, payload }) {
    super(message)
    this.fileName = fileName
    this.payload = payload
  }
}

export const isMimeTypeAllowed = (unAllowedMimeTypesArr, positiveLock) => file => {
  let ext = file.name.split('.')
  ext = ext[ext.length - 1]
  const a = unAllowedMimeTypesArr.some(type => type === ext)
  return (positiveLock ? !a : a)
    ? new FileValidation({
        message: FILE_INPUT_ERROR_EXTENSION,
        fileName: file.name,
        payload: { mimeTypes: unAllowedMimeTypesArr, positiveLock },
      })
    : file
}

export const isFileSizeAllowed = fileSize => file =>
  file.size < fileSize
    ? file
    : new FileValidation({
        message: FILE_INPUT_ERROR_SIZE,
        fileName: file.name,
        payload: `${parseInt(fileSize, 10) * 1048576}MB`,
      })

const FileInputController = ({
  containerRef,
  id,
  multiple,
  allowedMIMETypes,
  unAllowedMimeTypes,
  value,
  onInput,
  children,
  uploadFunction,
  fileSize,
}) => {
  const [position, setPosition] = useState(0)
  const fileInputRef = useRef()
  const focusInput = useCallback(position => {
    if (typeof position === 'number') {
      setPosition(position)
    }
    fileInputRef.current.click()
  }, [])

  const [overflowedFiles, setOverflowedStatus] = useState(undefined)
  const [rejectedFiles, setRejectedFiles] = useState([])
  const [tempFiles, setTempFiles] = useState([])
  const refFileOverStatus = useRef(overflowedFiles)
  refFileOverStatus.current = overflowedFiles

  const updateValue = useCallback(
    value => {
      onInput(value, id)
    },
    [id, onInput],
  )

  const progressUpload = useCallback(
    files =>
      ({ loaded, total }) => {
        const filesLength = files.length
        const { f } = files.reduce(
          (acc, { size, ...file }, i) => {
            let progress =
              i + 1 === filesLength ? acc.loaded : acc.loaded * (size / acc.total + (filesLength - i) / 100)
            progress = progress > size ? size : progress > 0 ? progress : 1
            acc.loaded -= progress
            acc.total -= size
            acc.f.push({ ...file, size, progress: Math.round((progress * 100) / size) })
            return acc
          },
          { total, loaded, f: [] },
        )
        setTempFiles(prevTempFiles => prevTempFiles.map(file => f.find(newFile => newFile.id === file.id) || file))
      },
    [],
  )

  const uploadFiles = useCallback(
    async files => {
      try {
        const uploadedFiles = await uploadFunction(files, { onUploadProgress: progressUpload(files) })
        setTempFiles(prevTempFiles => {
          const nextTempFiles = [...prevTempFiles]
          let position
          files.forEach(file => {
            position = [nextTempFiles.splice(nextTempFiles.indexOf(file), 1)].position - files.length - 1
          })
          if (position !== 0) {
            const nextValue = [...value]
            nextValue.splice(position, files.length, ...uploadedFiles)
            updateValue(nextValue)
          } else {
            updateValue(multiple ? [...value, ...uploadedFiles] : uploadedFiles)
          }
          return nextTempFiles
        })

        fileInputRef.current.value = ''
      } catch (e) {
        setTempFiles(prevTempFiles =>
          prevTempFiles.map(file =>
            files.find(newFile => newFile.id === file.id) ? { ...file, progress: 0, fail: e.message } : file,
          ),
        )
      }
    },
    [uploadFunction, progressUpload, value, updateValue, multiple],
  )

  const handleInput = useCallback(
    async ({ target: { files } }) => {
      const { allowed, rejected } = Object.values(files).reduce(
        (acc, file) => {
          new Pipe(
            file,
            value => acc.allowed.push(value),
            value => acc.rejected.push(value),
          ).apply([
            isMimeTypeAllowed(
              (allowedMIMETypes || unAllowedMimeTypes).map(v => (v.startsWith('.') ? v.slice(1) : v)),
              !!allowedMIMETypes,
            ),
            isFileSizeAllowed(fileSize),
          ])
          return acc
        },
        { allowed: [], rejected: [] },
      )

      if (rejected.length > 0) {
        setRejectedFiles(p => [...p, ...rejected])
      }
      if (allowed.length === 0) return

      const f = await Promise.all(
        allowed.map(
          (cur, i) =>
            new Promise(resolve => {
              const reader = new FileReader()
              reader.readAsDataURL(cur)
              reader.onloadend = () => {
                resolve({
                  fileData: cur,
                  id: uniqueId(),
                  value: cur.name,
                  progress: 0,
                  fail: '',
                  size: cur.size,
                  file: reader.result,
                  position: position + i,
                })
              }
            }),
        ),
      )
      if (position > 0) {
        updateValue(PureDeleteItems(value, position, f.length))
      }
      setTempFiles(prevTempFiles => (multiple ? [...prevTempFiles, ...f] : f))
      await uploadFiles(f)
    },
    [position, uploadFiles, allowedMIMETypes, unAllowedMimeTypes, fileSize, updateValue, value, multiple],
  )

  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.ondragover = e => {
        if (!refFileOverStatus.current) {
          const files = []
          for (const item of e.dataTransfer.items) {
            if (item.kind === 'file') {
              console.log(item)
              files.push(item.type)
            }
          }
          if (files.length > 0) {
            setOverflowedStatus(files)
          }
        }
        e.preventDefault()
        e.stopPropagation()
      }
      containerRef.current.ondragleave = e => {
        setOverflowedStatus(undefined)
        e.preventDefault()
        e.stopPropagation()
      }
      containerRef.current.ondrop = e => {
        e.preventDefault()
        e.stopPropagation()
        const files = []
        for (const item of e.dataTransfer.items) {
          if (item.kind === 'file') {
            files.push(item.getAsFile())
          }
        }
        setOverflowedStatus(undefined)
        return handleInput({ target: { files } })
      }
      return () => {
        if (containerRef.current) {
          containerRef.current.ondragover = null
          containerRef.current.ondragleave = null
          containerRef.current.ondrop = null
        }
      }
    }
  }, [handleInput, containerRef])

  const mergedValue = useMemo(() => {
    const tempVal = [...value]

    tempFiles.forEach(item => {
      if (item.position) {
        tempVal.splice(item.position, 0, item)
      } else {
        tempVal.push(item)
      }
    })
    return tempVal
  }, [tempFiles, value])

  const onDelete = useCallback(
    index => () => {
      const deletedValue = mergedValue[index]
      const tempIndex = tempFiles.findIndex(v => v.id === deletedValue.id)
      if (tempIndex >= 0) {
        setTempFiles(PureDeleteItems(tempFiles, tempIndex))
      } else {
        updateValue(
          PureDeleteItems(
            value,
            value.findIndex(v => v === deletedValue),
          ),
        )
      }
    },
    [mergedValue, tempFiles, updateValue, value],
  )

  const onReUpload = useCallback(
    index => () => {
      const deletedValue = mergedValue[index]
      return uploadFiles([tempFiles.findIndex(v => v.id === deletedValue.id)])
    },
    [mergedValue, tempFiles, uploadFiles],
  )

  return (
    <>
      {children({
        overflowedFiles,
        value: mergedValue,
        rejectedFiles,
        setRejectedFiles,
        openFileInput: focusInput,
        onDelete,
        onReUpload,
      })}
      <input className="hidden" id={id} type="file" onInput={handleInput} multiple={multiple} ref={fileInputRef} />
    </>
  )
}

FileInputController.propTypes = {
  uploadFunction: PropTypes.func.isRequired,
  containerRef: PropTypes.shape({
    current: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({ current: PropTypes.instanceOf(Element) })]),
  }),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  multiple: PropTypes.bool,
  allowedMIMETypes: PropTypes.array,
  unAllowedMimeTypes: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  onInput: PropTypes.func.isRequired,
  children: PropTypes.func.isRequired,
  fileSize: PropTypes.number,
}
FileInputController.defaultProps = {
  value: [],
  unAllowedMimeTypes: [],
  allowedMIMETypes: undefined,
  containerRef: undefined,
  multiple: false,
  fileSize: 262144000,
}

export default FileInputController
