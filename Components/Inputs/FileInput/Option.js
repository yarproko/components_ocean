import PropTypes from 'prop-types'
import Icon from '../../Icon'
import UploadIcon from '../../../Icons/uploadIcon'
import Preloader from '../../../Icons/preloader'
import close from '../../../Icons/close'

import { OptionContainer, PreloaderContainer, RemoveButton, ReUploadFileButton } from './style'

const UploadingOption = ({ value: { name, progress, fail }, onReUpload, onDelete }) => {
  return (
    <OptionContainer progress={progress} fail={fail}>
      {name}
      {progress && !fail && (
        <PreloaderContainer>
          <Icon icon={Preloader} size={18} />
        </PreloaderContainer>
      )}
      {fail && (
        <ReUploadFileButton title="retry upload" type="button" onClick={onReUpload}>
          <Icon icon={UploadIcon} />
        </ReUploadFileButton>
      )}
      <RemoveButton type="button" onClick={onDelete}>
        <Icon icon={close} size={6} />
      </RemoveButton>
    </OptionContainer>
  )
}

UploadingOption.propTypes = {
  value: PropTypes.shape({
    name: PropTypes.string.isRequired,
    progress: PropTypes.number.isRequired,
    fail: PropTypes.bool,
  }).isRequired,
  onReUpload: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
}

const UploadedOption = ({ value, onDelete }) => (
  <OptionContainer>
    {value}
    <RemoveButton type="button" onClick={onDelete}>
      <Icon icon={close} size={6} />
    </RemoveButton>
  </OptionContainer>
)

UploadedOption.propTypes = {
  value: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
}

const Option = ({ value, ...props }) => {
  return typeof value === 'string' ? (
    <UploadedOption value={value} {...props} />
  ) : (
    <UploadingOption value={value} {...props} />
  )
}

Option.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
}

export default Option
