import { useRef } from 'react'
import PropTypes from 'prop-types'
import FileInputController from './FileInputController'
import BaseFileInputComponent from './BaseFileInputComponent'
import { FILE_INPUT_ERROR_EXTENSION, FILE_INPUT_ERROR_SIZE } from './constants'

export { FILE_INPUT_ERROR_EXTENSION, FILE_INPUT_ERROR_SIZE }

const FileInput = ({ className, inputComponent: Input, multiple, ...props }) => {
  const refContainer = useRef()
  return (
    <FileInputController {...props} containerRef={refContainer} multiple={multiple}>
      {fileInputProps => (
        <div className="contents" ref={refContainer}>
          <Input {...fileInputProps} className={className} multiple={multiple} />
        </div>
      )}
    </FileInputController>
  )
}

FileInput.propTypes = {
  className: PropTypes.string,
  inputComponent: PropTypes.elementType,
  multiple: PropTypes.bool,
}
FileInput.defaultProps = {
  className: '',
  multiple: false,
  inputComponent: BaseFileInputComponent,
}

export default FileInput
