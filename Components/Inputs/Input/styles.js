import styled from 'styled-components'

export const InputFillIndicator = styled.div`
  position: absolute;
  bottom: -1px;
  left: 0;
  height: 2px;
  background: var(--color-light-gold-1, #bfa764);
`
