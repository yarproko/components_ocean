import { forwardRef, useCallback, useEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { defaultRegexPlaceholders } from './constants'
import diff from 'fast-diff'

const deletedSymbol = Symbol('deleted')
// значения для тестов
// const pairs = [
//   ['7 (___) ___-__-__', '+7 (___) ___-__-__', '+7 (___) ___-__-__'],
//   ['+7 (1__) ___-__-__', '+7 (___) ___-__-__', '+7 (1__) ___-__-__'],
//   ['+7 (a) ___-__-__', '+7 (___) ___-__-__', '+7 (___) ___-__-__'],
//   ['+7 (a#___) ___-__-__', '+7 (___) ___-__-__', '+7 (___) ___-__-__'],
//   ['+7 (1___) ___-__-__', '+7 (___) ___-__-__', '+7 (1__) ___-__-__'],
//   ['+7 (124___) ___-__-__', '+7 (___) ___-__-__', '+7 (124) ___-__-__'],
//   ['1+7 (___) ___-__-__', '+7 (___) ___-__-__', '+7 (1__) ___-__-__'],
//   ['17 (___) ___-__-__', '+7 (___) ___-__-__', '+7 (1__) ___-__-__'],
//   ['1 ___-7_-__', '+7 (___) ___-7_-__', '+7 (1__) ___-7_-__'],
//   ['+7 (999) 99-99-99', '+7 (999) 999-99-99', '+7 (999) 99_-99-99'],
//   ['+7 (999) 999-99-99+7 (999) 99-99-99', '+7 (999) 999-99-99', '+7 (999) 999-99-99'],
//   ['1234+7 (999) 99-99-991231', '+7 (999) 999-99-99', '+7 (123) 499-99-99'],
//   ['+7 (123) 4+9-99-991231', '+7 (999) 999-99-99', '+7 (123) 4_9-99-99'],
//   // ['тлираалилисаба', 'алилиса', '+7 (___) ___-__-__'],
//   ['+7 (999) 8-99', '+7 (999) 999-99-99', '+7 (999) 8__-__-99'],
//   ['+7 (999) 9-99', '+7 (999) 999-99-99', '+7 (999) 9__-__-99'],
//   ['+7 (998889-99', '+7 (999) 999-99-99', '+7 (998) 88_-_9-99'],
//   ['99', '+7 (999) 999-99-99', '+7 (99_) ___-__-__'],
//   ['78', '+7 (123) 456-78-91', '+7 (78_) ___-__-__'],
//   ['', '+7 (999) 999-99-99', '+7 (___) ___-__-__'],
// ]
const Mask = forwardRef(
  ({ value, placeholderSymbol, id, inputComponent: InputComponent, mask, onInput, onFocus, onBlur, ...props }, ref) => {
    const inputRef = useRef()
    const valueRef = useRef()
    const prevValueRef = useRef()
    const prevMaskRef = useRef()
    const [focused, setFocused] = useState(false)
    const [innerValue, setInnerValue] = useState('')
    const refHandler = useCallback(
      object => {
        if (object) {
          if (ref) {
            ref.current = object
          }
          inputRef.current = object
        }
      },
      [ref],
    )

    const normalizedMask = useMemo(() => {
      const symbolsArray = Array.isArray(mask) ? mask : mask.split('').map(s => defaultRegexPlaceholders[s] || s)
      return symbolsArray.reduce((acc, s, i) => {
        acc.set(i, s)
        return acc
      }, new Map())
    }, [mask])

    const minPointerPosition = useMemo(() => {
      let i = 0
      for (const { 1: mask } of normalizedMask) {
        if (mask instanceof RegExp) {
          break
        }
        i++
      }
      return i
    }, [normalizedMask])

    useEffect(() => {
      if (focused) {
        if (value !== valueRef.current || valueRef.current === undefined || normalizedMask !== prevMaskRef.current) {
          valueRef.current = value
          const maskedValue = []
          normalizedMask.forEach((mask, index) => {
            if (mask instanceof RegExp) {
              maskedValue.push(mask.test(value[index]) ? value[index] : placeholderSymbol)
            } else {
              maskedValue.push(mask)
            }
          })
          const val = maskedValue.join('')
          inputRef.current.value = val
          let selectionRange
          if (normalizedMask !== prevMaskRef.current) {
            selectionRange = inputRef.current.selectionStart
          } else {
            selectionRange = maskedValue.indexOf(placeholderSymbol)
          }
          inputRef.current.setSelectionRange(selectionRange, selectionRange)
          setInnerValue(val)
          prevValueRef.current = val
          prevMaskRef.current = normalizedMask
        }
      } else {
        setInnerValue(value)
      }
    }, [normalizedMask, placeholderSymbol, value, focused])

    const handleInput = useCallback(
      (value, ...params) => {
        const inputDiff = diff(prevValueRef.current, value)
        const { rawValue, insertedValues } =
          inputDiff.reduce((acc, [, b]) => `${acc}${b}`, '') === prevValueRef.current &&
          prevValueRef.current.indexOf(value) >= 0 &&
          inputRef.current.selectionStart !== 0
            ? // Покрываем случай, когда ввели новую строку вместо старой и она совпадает с концом старой
              {
                rawValue: Array(normalizedMask.size).fill(deletedSymbol),
                insertedValues: new Map([[0, value.split('')]]),
              }
            : inputDiff.reduce(
                (acc, [diff, str]) => {
                  if (diff === 1) {
                    acc.insertedValues.set(acc.pointer, str.split(''))
                    acc.pointer += str.length
                  } else if (diff === 0) {
                    acc.rawValue.push(...str.split(''))
                    acc.pointer += str.length
                  } else {
                    acc.rawValue.push(...Array(str.length).fill(deletedSymbol))
                  }
                  return acc
                },
                {
                  rawValue: [],
                  insertedValues: new Map(),
                  pointer: 0,
                },
              )
        let selectionRangeOffset = inputRef.current.selectionStart
        let result = ''
        for (let i = 0; i < rawValue.length; ) {
          const symbol = rawValue[i]
          const insertVal = insertedValues.get(i)
          let semiRes
          if (insertVal) {
            let noMaskCollision = true
            semiRes = ''
            insertVal.forEach(v => {
              let bypass = false
              while (!bypass && i < rawValue.length) {
                const mask = normalizedMask.get(i)
                if (mask instanceof RegExp) {
                  if (mask.test(v)) {
                    semiRes = `${semiRes}${v}`
                    selectionRangeOffset = i + 1
                  } else {
                    semiRes = `${semiRes}${placeholderSymbol}`
                    selectionRangeOffset = i
                  }
                  bypass = true
                } else {
                  semiRes = `${semiRes}${mask}`
                  if (mask === v && noMaskCollision) {
                    bypass = true
                  } else {
                    noMaskCollision = false
                  }
                }
                i++
              }
            })
          } else {
            if (symbol === deletedSymbol) {
              const mask = normalizedMask.get(i)
              semiRes = mask instanceof RegExp ? placeholderSymbol : mask
            } else {
              semiRes = symbol
            }
            i++
          }
          result = `${result}${semiRes}`
        }
        const selectionRange = minPointerPosition <= selectionRangeOffset ? selectionRangeOffset : minPointerPosition
        prevValueRef.current = result
        inputRef.current.value = result
        inputRef.current.setSelectionRange(selectionRange, selectionRange)
        setInnerValue(result)
        let inputPointer
        for (let i = normalizedMask.size; i >= minPointerPosition; i--) {
          if (normalizedMask.get(i) instanceof RegExp && result[i] !== placeholderSymbol) {
            inputPointer = i
            break
          }
        }
        const outputResult = inputPointer ? result.slice(0, inputPointer + 1) : ''
        valueRef.current = outputResult
        onInput(outputResult, ...params)
      },
      [normalizedMask, onInput, placeholderSymbol, minPointerPosition],
    )

    const handleFocus = useCallback(
      (...params) => {
        setFocused(true)
        if (inputRef.current.selectionStart < minPointerPosition) {
          inputRef.current.setSelectionRange(minPointerPosition, minPointerPosition)
        }
        onFocus(...params)
      },
      [minPointerPosition, onFocus],
    )
    const handleBlur = useCallback(
      (...params) => {
        setFocused(false)
        setInnerValue(valueRef.current)
        valueRef.current = undefined
        onBlur(...params)
      },
      [onBlur],
    )

    return (
      <InputComponent
        ref={refHandler}
        {...props}
        id={id}
        onInput={handleInput}
        onFocus={handleFocus}
        onBlur={handleBlur}
        value={innerValue}
      />
    )
  },
)

Mask.propTypes = {
  inputComponent: PropTypes.any.isRequired,
  mask: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  id: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  placeholderSymbol: PropTypes.string,
}
Mask.defaultProps = {
  mask: [],
  value: '',
  inputComponent: 'input',
  onFocus: () => null,
  onBlur: () => null,
  placeholderSymbol: '_',
}

export default Mask
