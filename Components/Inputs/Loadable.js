import { useCallback, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'

const refsCache = new Map()

export const useLoadableCache = ({ id, refKey = id, optionsMap, valueKey, multiple, value, returnOption }) => {
  const refOptionsMap = useRef({})
  useMemo(() => {
    refOptionsMap.current = optionsMap
  }, [optionsMap])

  const cache = useMemo(() => {
    if (!refsCache.has(refKey)) {
      refsCache.set(refKey, new Map())
    }
    return refsCache.get(refKey)
  }, [refKey])

  const valueKeys = useMemo(() => {
    const reducer = returnOption ? v => v[valueKey] : v => v
    return value ? (multiple ? value : [value]).map(reducer) : []
  }, [value, multiple, returnOption, valueKey])

  useMemo(() => {
    const optionsMap = refOptionsMap.current

    valueKeys.forEach(key => {
      if (!cache.has(key) && optionsMap[key]) {
        cache.set(key, optionsMap[key])
      }
    })
    // Мы кэшируем опции, только при смене value, кэш нужен для
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueKeys])

  return { valueKeys, cache }
}

const WithLoadable = Component => {
  const Loadable = ({ loadFunction, options, ...props }) => {
    const { valueKey } = props
    const [loadedOptionsState, setLoadedOptionsState] = useState({
      options: [],
      optionsMap: {},
    })
    const [loading, setLoadingState] = useState(false)

    const loadRef = useCallback(
      async search => {
        setLoadingState(true)
        const nextOptions = await loadFunction(search)
        setLoadingState(false)
        const result = {
          options: nextOptions,
          optionsMap: nextOptions.reduce((acc, item) => {
            acc[item[valueKey]] = item
            return acc
          }, {}),
        }
        setLoadedOptionsState(result)
      },
      [loadFunction, valueKey],
    )

    const baseOptionsMap = useMemo(
      () =>
        options.reduce((acc, item) => {
          acc[item[valueKey]] = item
          return acc
        }, {}),
      [options, valueKey],
    )

    const optionsMap = useMemo(
      () => ({
        ...baseOptionsMap,
        ...loadedOptionsState.optionsMap,
      }),
      [baseOptionsMap, loadedOptionsState],
    )

    const { valueKeys, cache } = useLoadableCache({ ...props, optionsMap })

    const mergedOptions = useMemo(() => {
      const { options, optionsMap } = loadedOptionsState
      const nextOptions = [...options]
      valueKeys.forEach(key => {
        if (!optionsMap[key]) {
          nextOptions.push(cache.get(key) || { [valueKey]: key })
        }
      })
      return nextOptions
      // мы обновляем опции только при загрузке новых options или value. т.к. валуе может помнять кто-то извне
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [valueKeys, loadedOptionsState])

    return <Component {...props} options={mergedOptions} loading={loading} remoteMethod={loadRef} remote={true} />
  }

  Loadable.propTypes = {
    valueKey: PropTypes.string.isRequired,
    loadFunction: PropTypes.func.isRequired,
    options: PropTypes.array,
  }

  Loadable.defaultProps = {
    loadFunction: () => [],
    options: [],
  }

  return Loadable
}

export default WithLoadable
