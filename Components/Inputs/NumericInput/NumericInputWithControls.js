import { useEffect, useRef } from 'react'
import { IncrementButton, IncrementsContainer } from './styles'
import NumericInput from './NumericInput'
import PropTypes from 'prop-types'

let interval

const releaseInterval = () => {
  clearInterval(interval)
  window.removeEventListener('mouseup', releaseInterval)
}

const NumericInputWithControls = props => {
  const { id, value, onInput } = props
  const refValue = useRef(value)
  refValue.current = value
  const onIncrement = () => {
    window.addEventListener('mouseup', releaseInterval)
    interval = setInterval(() => onInput(Number(refValue.current) + 1, id), 55)
  }
  const onDecrement = () => {
    window.addEventListener('mouseup', releaseInterval)
    interval = setInterval(() => onInput(Number(refValue.current) - 1, id), 55)
  }

  useEffect(() => releaseInterval, [])

  return (
    <NumericInput {...props}>
      <IncrementsContainer>
        <IncrementButton className="rotate-180" size={8} onMouseDown={onIncrement} />
        <IncrementButton className="last" size={8} onMouseDown={onDecrement} />
      </IncrementsContainer>
    </NumericInput>
  )
}

NumericInputWithControls.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  value: PropTypes.number,
  onInput: PropTypes.func.isRequired,
}

NumericInputWithControls.defaultProps = {
  value: 0,
}

export default NumericInputWithControls
