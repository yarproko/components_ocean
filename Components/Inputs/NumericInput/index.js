import NumericInput from './NumericInput'
import NumericInputWithControls from './NumericInputWithControls'

export { NumericInputWithControls }

export default NumericInput
