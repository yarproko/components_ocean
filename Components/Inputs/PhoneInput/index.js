import PropTypes from 'prop-types'
import InputMask from '../InputMask'
import { Container } from '../styles'
import StyledHtmlInput from '@/component_ocean/Components/Inputs/StyledHtmlInput'

const PhoneInput = ({ className, disabled, ...props }) => {
  return (
    <Container className={`${className} flex items-center justify-center`} disabled={disabled}>
      <InputMask {...props} disabled={disabled} inputComponent={StyledHtmlInput} />
    </Container>
  )
}

PhoneInput.propTypes = {
  value: PropTypes.string,
  onInput: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  mask: PropTypes.string,
}

PhoneInput.defaultProps = {
  mask: '+7 (999) 999-99-99',
  value: '',
  placeholder: '',
  className: '',
  disabled: false,
  onFocus: () => null,
  onBlur: () => null,
}

export default PhoneInput
