import { useCallback, useMemo } from 'react'
import PropTypes from 'prop-types'
import { BoxContainer, Button } from './style'

const RadioButton = ({
  text,
  className,
  id,
  onBlur,
  onFocus,
  onInput,
  value,
  returnObjects,
  buttonValue,
  valueKey,
}) => {
  const radioButtonMean = useMemo(
    () => (returnObjects ? buttonValue : buttonValue[valueKey]),
    [returnObjects, buttonValue, valueKey],
  )

  const selected = useMemo(
    () =>
      typeof radioButtonMean === 'object' ? radioButtonMean[valueKey] === value[valueKey] : radioButtonMean === value,
    [value, valueKey, radioButtonMean],
  )

  const updateValue = useCallback(() => {
    onFocus(id)
    onInput(radioButtonMean, id)
    onBlur(id)
  }, [radioButtonMean, id, onFocus, onBlur, onInput])

  return (
    <button onMouseDown={updateValue} className={`${className} flex items-center select-none`} type="button">
      <BoxContainer>
        <Button checked={selected} />
      </BoxContainer>
      <div className="pl-4">{text}</div>
    </button>
  )
}

RadioButton.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onInput: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.number]),
  returnObjects: PropTypes.bool,
  buttonValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]).isRequired,
  valueKey: PropTypes.string,
}

RadioButton.defaultProps = {
  onBlur: () => null,
  onFocus: () => null,
  className: '',
  style: {},
  returnObjects: true,
  text: '',
  value: undefined,
  valueKey: undefined,
}

export default RadioButton
