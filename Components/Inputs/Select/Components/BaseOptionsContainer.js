import PropTypes from 'prop-types'
import Option from './Option'
import { useEffect, useMemo, useRef, useState } from 'react'
import SimpleBar from 'simplebar-react'
import styled from 'styled-components'
import NoOptionsComponent from './NoOptionsComponent'

const ScrollBar = styled(SimpleBar)`
  max-height: 150px;
`

const BaseOptionsContainer = ({
  options,
  valueKey,
  value,
  labelKey,
  onInput,
  onCloseSelect,
  className,
  optionComponent: OptionComponent,
  noOptionsComponent: NoOptionsComponent,
}) => {
  const [highlightedOption, setHighlightedOption] = useState(0)
  const scrollableNodeRef = useRef()

  useEffect(() => {
    const keyHandlers = {
      Esc: onCloseSelect,
      ArrowUp: e => {
        e.preventDefault()
        setHighlightedOption(prevOption => {
          if (prevOption > 0) {
            const {
              children: [{ children }],
            } = scrollableNodeRef.current
            scrollableNodeRef.current.scrollTop = children[prevOption - 1].offsetTop

            return prevOption - 1
          }

          return 0
        })
      },
      ArrowDown: e => {
        e.preventDefault()
        setHighlightedOption(prevOption => {
          if (prevOption < options.length - 1) {
            const {
              children: [{ children }],
            } = scrollableNodeRef.current
            scrollableNodeRef.current.scrollTop = children[prevOption + 1].offsetTop

            return prevOption + 1
          }

          return options.length - 1
        })
      },
      Enter: e => {
        e.stopPropagation()
        setHighlightedOption(prevOption => {
          onInput(options[prevOption])
          return 0
        })
      },
      Tab: e => {
        e.stopPropagation()
        setHighlightedOption(prevOption => {
          onInput(options[prevOption])
          return 0
        })
      },
    }
    const listener = e => {
      if (keyHandlers[e.key]) {
        keyHandlers[e.key](e)
      }
    }
    window.addEventListener('keydown', listener)
    return () => {
      window.removeEventListener('keydown', listener)
    }
  }, [onCloseSelect, onInput, options])

  return (
    <ScrollBar scrollableNodeProps={useMemo(() => ({ ref: scrollableNodeRef }), [])} className={className}>
      {options.length > 0 ? (
        options.map((option, index) => (
          <OptionComponent
            onSelect={onInput}
            key={option[valueKey]}
            option={option}
            selectedOptions={value}
            labelKey={labelKey}
            valueKey={valueKey}
            index={index}
            highlightedOption={highlightedOption}
            onUpdateHighlightedOption={setHighlightedOption}
          />
        ))
      ) : (
        <NoOptionsComponent />
      )}
    </ScrollBar>
  )
}

BaseOptionsContainer.propTypes = {
  options: PropTypes.array,
  valueKey: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array]),
  labelKey: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  onCloseSelect: PropTypes.func.isRequired,
  className: PropTypes.string,
  optionComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  noOptionsComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
}

BaseOptionsContainer.defaultProps = {
  options: [],
  className: '',
  optionComponent: Option,
  value: undefined,
  noOptionsComponent: NoOptionsComponent,
}

export default BaseOptionsContainer
