import { forwardRef, useCallback, useMemo } from 'react'
import PropTypes from 'prop-types'
import { Input } from '../../styles'

const BaseSelectInput = forwardRef(
  ({ id, value, isSelectOpen, labelKey, searchValue, onSearch, placeholder, disabled, options, valueKey }, ref) => {
    const handleInput = useCallback(
      ({ target: { value }, target }) => {
        if (!disabled) {
          onSearch(value)
        } else {
          target.value = ''
        }
      },
      [disabled, onSearch],
    )

    const selectedLabel = useMemo(() => {
      if (value) {
        return (value === 'object' ? value : options.find(v => v[valueKey] === value))[labelKey]
      } else {
        return ''
      }
    }, [labelKey, options, value, valueKey])

    return (
      <Input
        id={id}
        className="overflow-hidden"
        type="text"
        ref={ref}
        disabled={disabled}
        onInput={handleInput}
        value={!isSelectOpen ? selectedLabel : searchValue}
        placeholder={selectedLabel || placeholder}
      />
    )
  },
)

BaseSelectInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  isSelectOpen: PropTypes.bool,
  labelKey: PropTypes.string.isRequired,
  searchValue: PropTypes.string,
  id: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
  valueKey: PropTypes.string.isRequired,
}

BaseSelectInput.defaultProps = {
  value: undefined,
  options: [],
  isSelectOpen: false,
  disabled: false,
  searchValue: '',
  placeholder: '',
}

export default BaseSelectInput
