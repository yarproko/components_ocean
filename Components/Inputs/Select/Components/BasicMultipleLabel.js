import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { useContext } from 'react'
import { MultipleSelectOptionsContext } from '../contants'

const OthersContainer = styled.span``

const Container = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 16px;
  padding: inherit;
  white-space: nowrap;
  overflow: hidden;
  display: flex;
  align-items: center;
  pointer-events: none;
  ${({ isSelectOpen }) =>
    isSelectOpen
      ? css`
          color: var(--input-placeholder-color, #c4c4c4);
        `
      : css`
          ${OthersContainer} {
            color: var(--text-secondary);
          }
        `};
`

const BasicMultipleLabel = ({ value: [v, ...others], labelKey, suffixText, isSelectOpen }) => {
  const { selectedLabels } = useContext(MultipleSelectOptionsContext)
  return (
    <Container isSelectOpen={isSelectOpen}>
      <span className="overflow-hidden">{typeof v === 'object' ? v[labelKey] : selectedLabels.get(v)}</span>
      {others.length > 0 && (
        <OthersContainer className="font-size-10 ml-2">{suffixText(others.length)}</OthersContainer>
      )}
    </Container>
  )
}

BasicMultipleLabel.propTypes = {
  suffixText: PropTypes.func,
  value: PropTypes.array,
  labelKey: PropTypes.string.isRequired,
  isSelectOpen: PropTypes.bool,
}

BasicMultipleLabel.defaultProps = {
  value: [],
  isSelectOpen: false,
  suffixText: count => `(+ ${count} других)`,
}

export default BasicMultipleLabel
