import PropTypes from 'prop-types'
import BaseOptionsContainer from './BaseOptionsContainer'
import MultipleOption from './MultipleOption'
import { SelectedOptionsScrollBar } from '../styles'

const BasicMultipleOptionContainer = props => {
  const { value, valueKey } = props
  return (
    <div>
      <BaseOptionsContainer {...props} />
      {value.length > 0 && (
        <SelectedOptionsScrollBar>
          <div className="flex flex-wrap items-center">
            {value.map((v, index) => (
              <MultipleOption key={typeof v === 'object' ? v[valueKey] : v} index={index} {...props} value={v} />
            ))}
          </div>
        </SelectedOptionsScrollBar>
      )}
    </div>
  )
}

BasicMultipleOptionContainer.propTypes = {
  value: PropTypes.array,
  valueKey: PropTypes.string.isRequired,
}

BasicMultipleOptionContainer.defaultProps = {
  value: [],
}
export default BasicMultipleOptionContainer
