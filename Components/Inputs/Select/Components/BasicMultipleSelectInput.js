import { forwardRef, useCallback } from 'react'
import PropTypes from 'prop-types'
import { Input } from '../../styles'
import BasicMultipleLabel from './BasicMultipleLabel'

const BasicMultipleSelectInput = forwardRef(
  ({ id, value, isSelectOpen, labelKey, searchValue, onSearch, placeholder, disabled }, ref) => {
    const handleInput = useCallback(
      ({ target: { value }, target }) => {
        if (!disabled) {
          onSearch(value)
        } else {
          target.value = ''
        }
      },
      [disabled, onSearch],
    )

    return (
      <>
        <Input
          className="overflow-hidden"
          type="text"
          ref={ref}
          id={id}
          disabled={disabled}
          onInput={handleInput}
          value={!isSelectOpen ? '' : searchValue}
          placeholder={value.length > 0 ? '' : placeholder}
        />
        {searchValue.length === 0 && (
          <BasicMultipleLabel isSelectOpen={isSelectOpen} labelKey={labelKey} value={value} />
        )}
      </>
    )
  },
)

BasicMultipleSelectInput.propTypes = {
  value: PropTypes.array,
  isSelectOpen: PropTypes.bool,
  labelKey: PropTypes.string.isRequired,
  searchValue: PropTypes.string,
  onSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
}

BasicMultipleSelectInput.defaultProps = {
  value: [],
  isSelectOpen: false,
  disabled: false,
  searchValue: '',
  placeholder: '',
}

export default BasicMultipleSelectInput
