import PropTypes from 'prop-types'
import ContextMenu from '../../../ContextMenu'
import { DropDownContentContainer } from '../../styles'

const DropDownComponent = ({ isOpen, children, ...props }) =>
  isOpen && (
    <ContextMenu {...props}>
      <DropDownContentContainer className="flex flex-col bg-white mt-1 rounded select-none">
        {children}
      </DropDownContentContainer>
    </ContextMenu>
  )

DropDownComponent.propTypes = {
  // eslint-disable-next-line react/require-default-props
  isOpen: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
}

export default DropDownComponent
