import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { MultipleSelectOptionsContext } from '../contants'
import Icon from '../../../Icon'
import removeIcon from '../../../../Icons/removeIcon'
import { MultipleOptionContainer } from '../styles'

const MultipleOption = ({ disabled, value, valueKey, index, style }) => {
  const { selectedLabels, handleDeselect } = useContext(MultipleSelectOptionsContext)
  return (
    <MultipleOptionContainer style={style}>
      {selectedLabels.get(typeof value === 'object' ? value[valueKey] : value)}
      <button disabled={disabled} type="button" onMouseDown={handleDeselect(index)}>
        <Icon icon={removeIcon} className="ml-2.5" size="8" />
      </button>
    </MultipleOptionContainer>
  )
}

MultipleOption.propTypes = {
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  valueKey: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  style: PropTypes.object,
}

MultipleOption.defaultProps = {
  value: '',
  disabled: false,
  style: {},
}

export default React.memo(MultipleOption)
