import { useContext } from 'react'
import { NoOptionsMessage } from '../contants'

const NoOptionsComponent = () => {
  const message = useContext(NoOptionsMessage)
  return <div className="p-2">{message}</div>
}

export default NoOptionsComponent
