import { useCallback, useMemo } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const OptionContainer = styled.div`
  font-size: 14px;
  line-height: 1.42857143; /* Normalize line height */
  display: block;
  padding: 5px 15px;
  clear: both;
  white-space: normal;
  cursor: pointer;
  transition: color, background-color ease-in-out 250ms;
  ${props => props.selected && 'color: var(--elements-hover-color, #BFA764);'}
  ${props => props.highlited && 'background-color: #f2f2f2;'}
`

const useOptionSelected = ({ valueKey, option: { [valueKey]: value }, selectedOptions }) =>
  useMemo(
    () => (typeof selectedOptions === 'object' ? selectedOptions[valueKey] : selectedOptions) === value,
    [selectedOptions, value, valueKey],
  )

const Option = props => {
  const {
    labelKey,
    index,
    highlightedOption,
    useOptionSelected,
    option,
    option: { [labelKey]: label },
    onSelect,
    onUpdateHighlightedOption,
  } = props

  const isOptionSelected = useOptionSelected(props)

  const handleSelect = useCallback(() => {
    onSelect(option, isOptionSelected)
  }, [isOptionSelected, onSelect, option])

  const handleUpdateTypePointer = useCallback(() => {
    onUpdateHighlightedOption(index)
  }, [index, onUpdateHighlightedOption])

  return (
    <OptionContainer
      selected={isOptionSelected}
      highlited={index === highlightedOption}
      onMouseDown={handleSelect}
      onMouseOver={handleUpdateTypePointer}
    >
      {label}
    </OptionContainer>
  )
}

Option.propTypes = {
  labelKey: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  highlightedOption: PropTypes.number.isRequired,
  selectedOptions: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array]),
  useOptionSelected: PropTypes.func,
  valueKey: PropTypes.string.isRequired,
  option: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  onUpdateHighlightedOption: PropTypes.func.isRequired,
}

Option.defaultProps = {
  selectedOptions: undefined,
  useOptionSelected,
}

export default Option
