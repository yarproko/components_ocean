import { forwardRef, useCallback, useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { Input } from '../../styles'
import MultipleOption from './MultipleOption'

const RenderMultipleValueSelectInput = forwardRef(
  ({ value, valueKey, searchValue, onSearch, placeholder, disabled, id }, ref) => {
    const [elementStyle, setElementSizes] = useState({})
    const container = useRef()

    const handleInput = useCallback(
      ({ target: { value }, target }) => {
        if (!disabled) {
          onSearch(value)
        } else {
          target.value = ''
        }
      },
      [disabled, onSearch],
    )

    useEffect(() => {
      const a = window.getComputedStyle(container.current.offsetParent, null)
      setElementSizes({
        height: `calc(${a.getPropertyValue('min-height')} - ${a.getPropertyValue('padding-top')} - ${a.getPropertyValue(
          'padding-bottom',
        )}`,
      })
    }, [])

    return (
      <div className="flex flex-wrap items-center w-full overflow-hidden" ref={container}>
        {value.map((v, index) => (
          <MultipleOption
            key={typeof v === 'object' ? v[valueKey] : v}
            index={index}
            disabled={disabled}
            valueKey={valueKey}
            value={v}
            style={elementStyle}
          />
        ))}
        <Input
          id={id}
          className="ml-0.5 mr-1.5 overflow-hidden"
          type="text"
          ref={ref}
          style={elementStyle}
          disabled={disabled}
          onInput={handleInput}
          value={searchValue}
          placeholder={value.length === 0 ? placeholder : ''}
        />
      </div>
    )
  },
)

RenderMultipleValueSelectInput.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.array,
  searchValue: PropTypes.string,
  onSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  valueKey: PropTypes.string.isRequired,
}

RenderMultipleValueSelectInput.defaultProps = {
  value: [],
  disabled: false,
  searchValue: '',
  placeholder: '',
}

export default RenderMultipleValueSelectInput
