import { forwardRef, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import memoize from 'lodash/memoize'

const FilterSelect = memoize(Component => {
  const FilterSelect = forwardRef((props, ref) => {
    const { options, labelKey } = props
    const [inputValue, setInputValue] = useState('')

    const filteredOptions = useMemo(
      () =>
        options.filter(({ [labelKey]: label }) =>
          label
            ? (typeof label !== 'string' ? label.toString() : label).toLowerCase().indexOf(inputValue.toLowerCase()) >
              -1
            : false,
        ),
      [inputValue, labelKey, options],
    )
    return (
      <Component ref={ref} {...props} options={filteredOptions} searchValue={inputValue} onSearch={setInputValue} />
    )
  })

  FilterSelect.propTypes = {
    labelKey: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    ),
  }

  FilterSelect.defaultProps = {
    options: [],
  }

  return FilterSelect
})

export default FilterSelect
