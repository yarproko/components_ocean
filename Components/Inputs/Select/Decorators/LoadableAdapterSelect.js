import memoize from 'lodash/memoize'
import { forwardRef, useCallback } from 'react'
import PropTypes from 'prop-types'
import { NoOptionsMessage } from '../contants'

const LoadableAdapterSelect = memoize(Component => {
  const LoadableSelect = forwardRef((props, ref) => {
    const { onSearch, searchValue, remoteMethod, onOpenSelect, loading } = props

    const handleOpenSelect = useCallback(
      value => {
        if (value) {
          remoteMethod(searchValue)
        } else {
          onSearch('')
        }
        onOpenSelect(value)
      },
      [onOpenSelect, onSearch, remoteMethod, searchValue],
    )

    const handleSearch = useCallback(
      value => {
        remoteMethod(value)
        onSearch(value)
      },
      [onSearch, remoteMethod],
    )
    return (
      <NoOptionsMessage.Provider
        value={loading ? 'Загрузка...' : !searchValue ? 'Начните искать' : 'Ничего не найденно'}
      >
        <Component ref={ref} {...props} onSearch={handleSearch} onOpenSelect={handleOpenSelect} />
      </NoOptionsMessage.Provider>
    )
  })

  LoadableSelect.propTypes = {
    onSearch: PropTypes.func.isRequired,
    searchValue: PropTypes.string,
    loading: PropTypes.bool,
    remoteMethod: PropTypes.func.isRequired,
    onOpenSelect: PropTypes.func.isRequired,
  }

  LoadableSelect.defaultProps = {
    searchValue: '',
    loading: false,
  }

  return LoadableSelect
})

export default LoadableAdapterSelect
