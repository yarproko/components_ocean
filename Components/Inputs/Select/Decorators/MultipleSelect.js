import { forwardRef, useCallback, useMemo } from 'react'
import PropTypes from 'prop-types'
import memoize from 'lodash/memoize'
import PureDeleteItems from '../../../../Utils/Arrays/PureDeleteItems'
import BasicMultipleSelectInput from '../Components/BasicMultipleSelectInput'
import { MultipleSelectOptionsContext } from '../contants'
import BasicMultipleOptionContainer from '../Components/BasicMultipleOptionContainer'

const MultipleSelect = memoize(Component => {
  const MultipleSelect = forwardRef((props, ref) => {
    const { options, labelKey, valueKey, value, onInput, returnObjects, id } = props

    const selectedOptionsState = useMemo(
      () =>
        value.reduce(
          (acc, v) => {
            const normalizedValue = typeof v === 'object' ? v[valueKey] : v
            acc.selectedOptions.add(normalizedValue)
            acc.selectedLabels.set(
              normalizedValue,
              options.find(({ [valueKey]: optionValue }) => optionValue === normalizedValue)[labelKey],
            )
            return acc
          },
          {
            selectedOptions: new Set(),
            selectedLabels: new Map(),
          },
        ),
      [labelKey, options, value, valueKey],
    )

    const filteredOptions = useMemo(
      () =>
        options.filter(({ [valueKey]: optionValue }) => {
          let passed = true
          for (const value of selectedOptionsState.selectedOptions) {
            if (optionValue === value) {
              passed = false
              break
            }
          }
          return passed
        }),
      [options, selectedOptionsState.selectedOptions, valueKey],
    )

    const handleInput = useCallback(
      (option, id) => {
        if (option) {
          // по умолчанию уже возвращается option[valueKey]
          const selectedOptionValue = returnObjects ? option[valueKey] : option
          if (selectedOptionsState.selectedOptions.has(selectedOptionValue)) {
            onInput(
              PureDeleteItems(
                value,
                value.findIndex(
                  returnObjects
                    ? ({ [valueKey]: optionValue }) => selectedOptionValue === optionValue
                    : optionValue => selectedOptionValue === optionValue,
                ),
              ),
              id,
            )
          } else {
            onInput([...value, option], id)
          }
        } else {
          onInput(option, id)
        }
      },
      [onInput, returnObjects, selectedOptionsState.selectedOptions, value, valueKey],
    )

    return (
      <MultipleSelectOptionsContext.Provider
        value={useMemo(
          () => ({
            ...selectedOptionsState,
            handleDeselect: index => e => {
              e.stopPropagation()
              onInput(PureDeleteItems(value, index), id)
            },
          }),
          [id, onInput, selectedOptionsState, value],
        )}
      >
        <Component ref={ref} {...props} options={filteredOptions} onInput={handleInput} />
      </MultipleSelectOptionsContext.Provider>
    )
  })

  MultipleSelect.propTypes = {
    value: PropTypes.array,
    valueKey: PropTypes.string.isRequired,
    labelKey: PropTypes.string.isRequired,
    onInput: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    returnObjects: PropTypes.bool,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    ),
  }

  MultipleSelect.defaultProps = {
    returnObjects: false,
    value: [],
    options: [],
    closeOnSelect: false,
    inputComponent: BasicMultipleSelectInput,
    optionsComponent: BasicMultipleOptionContainer,
  }

  return MultipleSelect
})

export default MultipleSelect
