import PropTypes from 'prop-types'
import MultipleSelect from './MultipleSelect'
import SelectComponent from '../SelectComponent'
import { useState } from 'react'

const BaseMultipleSelect = MultipleSelect(SelectComponent)

export { SelectComponent, BaseMultipleSelect }

const SelectViewSelector = ({ multiple, multipleSelectComponent, singleSelectComponent, ...props }) => {
  const Comp = multiple ? multipleSelectComponent : singleSelectComponent
  const [inputValue, setInputValue] = useState('')
  const [isSelectOpen, setSelectState] = useState(false)

  return (
    <Comp
      {...props}
      searchValue={inputValue}
      onSearch={setInputValue}
      isSelectOpen={isSelectOpen}
      onOpenSelect={setSelectState}
    />
  )
}

SelectViewSelector.propTypes = {
  multiple: PropTypes.bool,
  multipleSelectComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  singleSelectComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
}

SelectViewSelector.defaultProps = {
  multiple: false,
  multipleSelectComponent: BaseMultipleSelect,
  singleSelectComponent: SelectComponent,
}

export default SelectViewSelector
