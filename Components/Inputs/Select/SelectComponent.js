import { useCallback, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { Container } from '../styles'
import BaseOptionsContainer from './Components/BaseOptionsContainer'
import BaseSelectInput from './Components/BaseSelectInput'
import { RemoveIconContainer } from './styles'
import { RemoveIcon, ToggleIndicatorIcon } from './icons'
import DropDownComponent from './Components/DropDownComponent'

const SelectComponent = ({
  inputComponent: InputComponent,
  optionsComponent: OptionsComponent,
  options,
  valueKey,
  labelKey,
  onInput,
  value,
  id,
  disabled,
  dropDownComponent: DropDownComponent,
  removeIconComponent: RemoveIconComponent,
  toggleIndicatorIconComponent: ToggleIndicatorIconComponent,
  searchValue,
  onSearch,
  className,
  placeholder,
  onFocus,
  onBlur,
  closeOnSelect,
  onOpenSelect,
  isSelectOpen,
  returnObjects,
}) => {
  const containerClickEventsRef = useRef()
  const [isHovered, setHoveredState] = useState(false)

  const toggleSelectState = useCallback(
    v => () => {
      if (!disabled) {
        onOpenSelect(v)
        if (v) {
          onFocus()
        } else {
          onBlur()
        }
      }
    },
    [disabled, onBlur, onFocus, onOpenSelect],
  )
  const handleInput = useCallback(
    v => {
      onInput(returnObjects ? v : v[valueKey], id)
      if (closeOnSelect) {
        onOpenSelect(false)
      }
    },
    [closeOnSelect, id, onInput, onOpenSelect, returnObjects, valueKey],
  )

  const onChangeHoveredState = useCallback(state => () => setHoveredState(state), [])

  const onContainerClick = useCallback(
    e => {
      // TODO: сломается в 18 и выше версиях.использовать Synthetic event
      containerClickEventsRef.current = e.nativeEvent
      if (!isSelectOpen) {
        toggleSelectState(true)()
      }
    },
    [isSelectOpen, toggleSelectState],
  )

  const handleCloseWindow = useCallback(
    e => {
      if (e !== containerClickEventsRef.current) {
        toggleSelectState(false)()
      }
    },
    [toggleSelectState],
  )

  return (
    <Container
      disabled={disabled}
      onMouseDown={onContainerClick}
      className={`${className} flex`}
      onMouseEnter={onChangeHoveredState(true)}
      onMouseLeave={onChangeHoveredState(false)}
    >
      <InputComponent
        id={id}
        disabled={disabled}
        value={value}
        valueKey={valueKey}
        labelKey={labelKey}
        isSelectOpen={isSelectOpen}
        setSelectState={toggleSelectState}
        searchValue={searchValue}
        onSearch={onSearch}
        options={options}
        placeholder={placeholder}
      />
      {isHovered && !(Array.isArray(value) ? value.length === 0 : !value) && !disabled ? (
        <RemoveIconContainer
          type="button"
          disabled={disabled}
          title="Delete"
          onMouseDown={() => handleInput(undefined)}
        >
          <RemoveIconComponent size="12" />
        </RemoveIconContainer>
      ) : (
        <ToggleIndicatorIconComponent onMouseDown={isSelectOpen ? toggleSelectState(false) : null} />
      )}
      <DropDownComponent isOpen={isSelectOpen} onClose={handleCloseWindow}>
        <OptionsComponent
          options={options}
          valueKey={valueKey}
          labelKey={labelKey}
          onInput={handleInput}
          value={value}
          onCloseSelect={toggleSelectState(false)}
        />
      </DropDownComponent>
    </Container>
  )
}

SelectComponent.propTypes = {
  inputComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  optionsComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  options: PropTypes.array,
  valueKey: PropTypes.string.isRequired,
  labelKey: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array, PropTypes.object]),
  id: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  dropDownComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  removeIconComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  toggleIndicatorIconComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  searchValue: PropTypes.string,
  onSearch: PropTypes.func.isRequired,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  closeOnSelect: PropTypes.bool,
  returnObjects: PropTypes.bool,
  onOpenSelect: PropTypes.func.isRequired,
  isSelectOpen: PropTypes.bool,
}

SelectComponent.defaultProps = {
  className: '',
  options: [],
  value: undefined,
  disabled: false,
  isSelectOpen: false,
  returnObjects: false,
  searchValue: '',
  placeholder: '',
  closeOnSelect: true,
  onBlur: () => null,
  onFocus: () => null,
  inputComponent: BaseSelectInput,
  optionsComponent: BaseOptionsContainer,
  dropDownComponent: DropDownComponent,
  removeIconComponent: RemoveIcon,
  toggleIndicatorIconComponent: ToggleIndicatorIcon,
}

export default SelectComponent
