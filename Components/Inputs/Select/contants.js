import React from 'react'

export const MultipleSelectOptionsContext = React.createContext({})
export const NoOptionsMessage = React.createContext('Ничего не найдено')
