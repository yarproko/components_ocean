import Icon from '../../Icon'
import removeIcon from '../../../Icons/removeIcon'
import toggleIndicatorIcon from '../../../Icons/toggleIndicator'

export const RemoveIcon = props => {
  return <Icon icon={removeIcon} {...props} />
}

export const ToggleIndicatorIcon = props => {
  return <Icon size="14" icon={toggleIndicatorIcon} {...props} />
}
