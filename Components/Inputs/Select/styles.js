import styled from 'styled-components'
import SimpleBar from 'simplebar-react'

export const RemoveIconContainer = styled.button`
  color: var(--color-grey-darken-0);
`

export const SelectedOptionsScrollBar = styled(SimpleBar)`
  max-height: 90px;
  border-top: 1px solid var(--color-grey-Light-4);
  margin-top: 3px;
  padding: 0 3px 3px;
`

export const MultipleOptionContainer = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  padding: 0 8px;
  height: calc(var(--form--elements_height, 38px) - 12px);
  min-width: 20px;
  margin: 1.5px 1.5px;
  background-color: #f2f2f2;
  width: min-content;
  border-radius: 2px;
  white-space: nowrap;
  transition: border-color 250ms ease-in-out;
`
