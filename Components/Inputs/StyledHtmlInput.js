import { forwardRef, useCallback } from 'react'
import PropTypes from 'prop-types'
import { Input as StyledInput } from './styles'

const Input = forwardRef(({ onInput, ...props }, ref) => {
  const handleManualInput = useCallback(
    ({ target: { value, id } }) => {
      onInput(value, id)
    },
    [onInput],
  )
  return <StyledInput ref={ref} onInput={handleManualInput} {...props} />
})

Input.propTypes = {
  onInput: PropTypes.func.isRequired,
}

export default Input
