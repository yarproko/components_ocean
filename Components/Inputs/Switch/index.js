import { useCallback } from 'react'
import PropTypes from 'prop-types'
import { Circle, ContainerSwitch, SwitchBlock, SwitchLabel } from './styles'

export { SwitchBlock, SwitchLabel, ContainerSwitch, Circle }

const Switch = ({ id, label, value, onInput, className, style }) => (
  <SwitchBlock
    onClick={useCallback(() => {
      onInput(!value, id)
    }, [id, onInput, value])}
    className={className}
    style={style}
  >
    <ContainerSwitch>
      <Circle activeSwitch={value} />
    </ContainerSwitch>
    <SwitchLabel>{label}</SwitchLabel>
  </SwitchBlock>
)

Switch.propTypes = {
  id: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
}

Switch.defaultProps = {
  label: '',
  value: undefined,
  className: '',
  style: undefined,
}

export default Switch
