import styled from 'styled-components'
import { Input } from '../styles'

export const TextArea = styled(Input.withComponent('textarea'))`
  height: ${({ textAreaHeight }) => textAreaHeight};
`
