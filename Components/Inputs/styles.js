import styled, { css } from 'styled-components'

export const Container = styled.div`
  position: relative;
  flex: var(--flex-input, 1 1 auto);
  width: var(--width-input, 100%);
  border: 1px solid var(--form-elements-border-color, #bdbdbd);
  border-radius: var(--form-elements-border-radius, 2px);
  background: var(--form-input-background-color, var(--background-primary, #fff));
  //height: var(--form--elements_height, 38px);
  min-height: var(--form--elements_height, 38px);
  height: min-content;
  padding: var(--padding-input, 5px 16px);
  ${({ disabled }) =>
    disabled &&
    css`
      color: var(--input-disabled-color, #bdbdbd);
      background: var(--input-disabled-bg, #f3f3f3);

      ${Input} {
        &::placeholder {
          color: var(--input-disabled-placeholder-color, #7a7a7a);
        }
      }
    `}
`

export const Input = styled.input`
  z-index: 10;
  font-size: 14px;
  background: inherit;
  flex-grow: 1;
  text-align: var(--text-aling-input, left);
  caret-color: var(--input-carret-color, #333333);
  resize: none;
  width: 100%;
  border-radius: var(--form-elements-border-radius, 2px);

  &::placeholder {
    color: var(--input-placeholder-color, #c4c4c4);
    opacity: 1;
    font-weight: 400;
    font-size: var(--plachodler-font-size, 12px);
  }

  &:focus {
    outline: none;
  }
`

export const DropDownContentContainer = styled.div`
  padding: 0;
  z-index: 1000;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.25);
`
