import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import { CloseIcon, DialogueBackground, DialogueContainer, ModalContainer } from './styles'
import { animated, useTransition } from 'react-spring'
import Icon from '../Icon'
import close from '../../Icons/close'

const animationSettings = {
  from: { opacity: 0 },
  enter: { opacity: 1 },
  leave: { opacity: 0 },
}

const ModalWindow = ({ children, onClose, className, open, closeIcon: CloseIcon }) => {
  const transitions = useTransition(open, animationSettings)

  return ReactDOM.createPortal(
    <div>
      {transitions(
        (style, item) =>
          item && (
            <animated.div style={style}>
              <ModalContainer>
                <DialogueBackground onClick={onClose} />
                <DialogueContainer className={className}>
                  <CloseIcon onClick={onClose} />

                  {children}
                </DialogueContainer>
              </ModalContainer>
            </animated.div>
          ),
      )}
    </div>,
    document.body,
  )
}

ModalWindow.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string,
  closeIcon: PropTypes.func,
}

ModalWindow.defaultProps = {
  className: '',
  closeIcon: props => (
    <CloseIcon {...props}>
      <Icon icon={close} size={12} />
    </CloseIcon>
  ),
}

export default ModalWindow
