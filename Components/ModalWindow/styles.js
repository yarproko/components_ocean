import styled from 'styled-components'

export const ModalContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;
`

export const DialogueBackground = styled.div`
  position: absolute;
  height: calc(100% + 50px);
  background-color: #000;
  opacity: 0.2;
  width: 100%;
`

export const DialogueContainer = styled.div`
  border: 1px solid var(--elements-active-color, #f3f3f3);
  background: var(--color-white);
  box-shadow: var(--shadow-3);
  z-index: 2;
  display: flex;
  padding: 20px;
  flex-direction: column;
  overscroll-behavior: contain;
  position: relative;
  overflow: hidden;
  margin-bottom: 20%;
  max-width: 80%;
  max-height: 80vh;
`

export const CloseIcon = styled.button`
  position: absolute;
  right: 8px;
  top: 8px;
  transition: 250ms linear color;
  color: var(--unfocused_elements, #bdbdbd);
  &:hover {
    color: var(--unfocused_elements_active, inherit);
  }
`
