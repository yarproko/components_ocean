import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import PerfectScrollbar from 'react-perfect-scrollbar'
import { ContainerContext } from '../../constants'

const StyledScrollBar = styled(PerfectScrollbar)`
  .ps__rail-x {
    z-index: 100;
  }
  .ps__rail-y {
    z-index: 100;
  }
  background: inherit;
`

const ScrollBar = (props, ref) => {
  const [container, setContainer] = useState({})
  const refHandler = useCallback(
    object => {
      if (object) {
        if (ref) {
          ref.current = object
        }
        // eslint-disable-next-line no-underscore-dangle
        setContainer(object._container)
      }
    },
    [ref],
  )
  return (
    <ContainerContext.Provider value={container}>
      <StyledScrollBar {...props} ref={refHandler} />
    </ContainerContext.Provider>
  )
}

export default React.forwardRef(ScrollBar)
