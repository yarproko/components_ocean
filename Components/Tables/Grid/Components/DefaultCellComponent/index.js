import PropTypes from 'prop-types'

const DefaultCellComponent = ({ value, style, className }) => {
  return (
    <div className={`${className} w-full h-full flex items-center px-2 my-auto`} style={style}>
      {value}
    </div>
  )
}

DefaultCellComponent.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  className: PropTypes.string,
}

DefaultCellComponent.defaultProps = {
  className: '',
  value: undefined,
  style: undefined,
}

export default DefaultCellComponent
