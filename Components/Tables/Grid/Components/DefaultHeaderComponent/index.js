import PropTypes from 'prop-types'

const DefaultCellComponent = ({ label, style, className }) => {
  return (
    <div className={`${className} w-full h-full flex items-center px-2 my-auto`} style={style}>
      {label}
    </div>
  )
}

DefaultCellComponent.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  className: PropTypes.string,
}

DefaultCellComponent.defaultProps = {
  className: '',
  label: '',
  style: undefined,
}

export default DefaultCellComponent
