import PropTypes from 'prop-types'
import { AutoSizer, ScrollSync, Grid as VirtualizedGrid } from 'react-virtualized'
import { Cell, GridBodyContainer, GridColumn, HeaderCell, LeftSideGridContainer, OverflowHiddenGrid } from './styles'
import scrollbarSize from 'dom-helpers/scrollbarSize'
import { useCallback, useLayoutEffect, useMemo, useRef, useState } from 'react'
import { useRecoilState } from 'recoil'
import { cachedLocalStorageValue } from '../../../Logic/Storages/localStorageCache'
import DefaultHeaderComponent from '../../../Components/Tables/Grid/Components/DefaultHeaderComponent'
import DefaultCellComponent from '../../../Components/Tables/Grid/Components/DefaultCellComponent'
import ApplyPlugins from '../../../Components/Tables/Plugins'

const columnSortFunc = (a, b) =>
  b.position !== undefined && a.position !== undefined ? (b.position > a.position ? -1 : 1) : 0

const Grid = ({ value, renderValue = value, id, columns, headerHeight }) => {
  // eslint-disable-next-line no-unused-vars
  const [columnState, setColumnState] = useRecoilState(cachedLocalStorageValue(`Grid${id}`))

  const columnsWithUiSettings = useMemo(() => {
    const state = columnState || {
      Year: { fixed: true },
      Year1: { fixed: true },
    }
    const { f, c } = columns.reduce(
      (acc, column) => {
        const { id, sizes = 150 } = column
        const { [id]: { fixed, width = sizes, hidden, fixedPosition, position } = {} } = state
        if (!hidden) {
          if (fixed) {
            acc.f.push({ columnWidth: width, position: fixedPosition, column })
          } else {
            acc.c.push({ columnWidth: width, position, column })
          }
        }
        return acc
      },
      { f: [], c: [] },
    )

    return {
      fixedColumnsState: f.sort(columnSortFunc),
      normalColumnsState: c.sort(columnSortFunc),
    }
  }, [columnState, columns])

  const { fixedColumnsState, normalColumnsState } = columnsWithUiSettings

  const refContainer = useRef()
  const [state, setState] = useState({
    overscanColumnCount: 0,
    overscanRowCount: 5,
    height: 0,
  })

  const { overscanColumnCount, overscanRowCount, height } = state

  useLayoutEffect(() => {
    const { clientHeight } = refContainer.current
    setState(s => ({ ...s, height: clientHeight }))
  }, [])

  const getColumnWidth = useCallback(
    ({ index }) => {
      return normalColumnsState[index].columnWidth
    },
    [normalColumnsState],
  )

  const getFixedColumnWidth = useCallback(
    ({ index }) => {
      return fixedColumnsState[index].columnWidth
    },
    [fixedColumnsState],
  )

  const fixedContainerWidth = useMemo(
    () => fixedColumnsState.reduce((acc, { columnWidth }) => acc + columnWidth, 0),
    [fixedColumnsState],
  )

  const renderLeftSideCell = useCallback(
    ({ columnIndex, key, rowIndex, style }) => {
      const {
        column: { id, component: Comp = DefaultCellComponent },
      } = fixedColumnsState[columnIndex]
      return (
        <Cell key={key} style={style}>
          <Comp id={id} value={renderValue[rowIndex][id]} ParentValue={renderValue[rowIndex]} />
        </Cell>
      )
    },
    [renderValue, fixedColumnsState],
  )

  const renderBodyCell = useCallback(
    ({ columnIndex, key, rowIndex, style }) => {
      const {
        column: { id, component: Comp = DefaultCellComponent },
      } = normalColumnsState[columnIndex]
      return (
        <Cell key={key} style={style}>
          <Comp id={id} value={renderValue[rowIndex][id]} ParentValue={renderValue[rowIndex]} />
        </Cell>
      )
    },
    [renderValue, normalColumnsState],
  )

  const renderLeftHeaderCell = useCallback(
    ({ columnIndex, key, style }) => {
      const {
        column: { label, id, headerCellComponent: Comp = DefaultHeaderComponent },
      } = fixedColumnsState[columnIndex]
      return (
        <HeaderCell key={key} style={style}>
          <Comp id={id} label={label} />
        </HeaderCell>
      )
    },
    [fixedColumnsState],
  )

  const renderHeaderCell = useCallback(
    ({ columnIndex, key, style }) => {
      const {
        column: { label, id, headerCellComponent: Comp = DefaultHeaderComponent },
      } = normalColumnsState[columnIndex]
      return (
        <HeaderCell key={key} style={style}>
          <Comp id={id} label={label} />
        </HeaderCell>
      )
    },
    [normalColumnsState],
  )

  const scrollSize = scrollbarSize()

  return (
    <div ref={refContainer} className="flex flex-col h-full overflow-hidden relative separator-left separator-top">
      <ScrollSync>
        {({ onScroll, scrollLeft, scrollTop }) => (
          <>
            <LeftSideGridContainer>
              <OverflowHiddenGrid
                className="w-full"
                cellRenderer={renderLeftHeaderCell}
                width={fixedContainerWidth}
                height={headerHeight + 1}
                rowHeight={headerHeight}
                columnWidth={getFixedColumnWidth}
                rowCount={1}
                columnCount={fixedColumnsState.length}
              />
              <OverflowHiddenGrid
                overscanColumnCount={overscanColumnCount}
                overscanRowCount={overscanRowCount}
                cellRenderer={renderLeftSideCell}
                columnWidth={getFixedColumnWidth}
                columnCount={fixedColumnsState.length}
                height={height - scrollSize - headerHeight}
                rowHeight={headerHeight}
                rowCount={renderValue.length}
                scrollTop={scrollTop}
                width={fixedContainerWidth}
              />
            </LeftSideGridContainer>
            <GridColumn>
              <AutoSizer disableHeight>
                {({ width }) => (
                  <GridBodyContainer marginLeft={fixedContainerWidth}>
                    <OverflowHiddenGrid
                      className="w-full"
                      columnWidth={getColumnWidth}
                      columnCount={normalColumnsState.length}
                      height={headerHeight + 1}
                      overscanColumnCount={overscanColumnCount}
                      cellRenderer={renderHeaderCell}
                      rowHeight={headerHeight}
                      rowCount={1}
                      scrollLeft={scrollLeft}
                      width={width - scrollSize - fixedContainerWidth}
                      right={scrollSize}
                    />
                    <VirtualizedGrid
                      className="w-full"
                      columnWidth={getColumnWidth}
                      columnCount={normalColumnsState.length}
                      height={height - headerHeight}
                      onScroll={onScroll}
                      overscanColumnCount={overscanColumnCount}
                      overscanRowCount={overscanRowCount}
                      cellRenderer={renderBodyCell}
                      rowHeight={headerHeight}
                      rowCount={renderValue.length}
                      width={width - fixedContainerWidth}
                    />
                  </GridBodyContainer>
                )}
              </AutoSizer>
            </GridColumn>
          </>
        )}
      </ScrollSync>
    </div>
  )
}

Grid.propTypes = {
  id: PropTypes.string.isRequired,
  columns: PropTypes.array,
  value: PropTypes.array,
  renderValue: PropTypes.array,
  headerHeight: PropTypes.number,
}

Grid.defaultProps = {
  value: [],
  columns: [],
  headerHeight: 40,
  renderValue: undefined,
}

export default ApplyPlugins(Grid)
