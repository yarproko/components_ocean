import styled from 'styled-components'
import { Grid } from 'react-virtualized'

export const LeftSideGridContainer = styled.div`
  flex: 0 0 100%;
  z-index: 10;
   {
    position: absolute;
    left: 0;
    top: 0;
    background: white;
  }
`
export const GridColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
`
export const GridBodyContainer = styled.div`
  margin-left: ${({ marginLeft }) => marginLeft}px;
`
export const OverflowHiddenGrid = styled(Grid)`
  overflow: hidden !important;
`

export const HeaderCell = styled.div`
  width: 100%;
  height: 100%;
  border-bottom: 1px solid #bdbdbd;
  border-right: 1px solid #bdbdbd;
`
export const Cell = styled.div`
  width: 100%;
  height: 100%;
  border-right: 1px solid #bdbdbd;
  border-bottom: 1px solid #bdbdbd;
`
