import PropTypes from 'prop-types'

const Cell = ({ value, className }) => <div className={`${className} word-wrap-anywhere`}>{value}</div>

Cell.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
}

Cell.defaultProps = {
  className: '',
  value: undefined,
}

export default Cell
