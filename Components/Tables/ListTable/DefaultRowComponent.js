import PropTypes from 'prop-types'
import { GridContainer } from './styles'

const DefaultRowComponent = ({ id, className, children, style }) => (
  <GridContainer id={id} className={className} style={style}>
    {children}
  </GridContainer>
)

DefaultRowComponent.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  style: PropTypes.object,
}

DefaultRowComponent.defaultProps = {
  id: '',
  className: '',
  children: null,
  style: undefined,
}

export default DefaultRowComponent
