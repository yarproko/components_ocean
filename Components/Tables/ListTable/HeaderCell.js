import PropTypes from 'prop-types'

const HeaderCell = ({ label }) => {
  return <div className="whitespace-nowrap">{label}</div>
}

HeaderCell.propTypes = {
  label: PropTypes.string,
}
HeaderCell.defaultProps = {
  label: '',
}

export default HeaderCell
