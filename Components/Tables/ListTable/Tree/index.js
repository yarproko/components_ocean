import PropTypes from 'prop-types'
import Icon from '../../../Icon'
import CircleMinus from './icons/CircleMinus'
import CirclePlus from './icons/CirclePlus'
import Dot from './icons/Dot'
import { IconContainer } from './styles'

const Tree = ({
  childrenKey,
  formPayload: { [childrenKey]: CHILDRENS },
  onToggleRenderNestedTable,
  renderNestedBranch,
}) => {
  const haveNestedData = CHILDRENS && Array.isArray(CHILDRENS) ? CHILDRENS.length > 0 : CHILDRENS
  return (
    <div className="flex items-center">
      <div className="mr-2.5">
        <IconContainer className="flex justify-center">
          <Icon
            icon={haveNestedData ? (renderNestedBranch ? CircleMinus : CirclePlus) : Dot}
            size={haveNestedData ? '12' : '4'}
            onClick={onToggleRenderNestedTable}
          />
        </IconContainer>
      </div>
    </div>
  )
}

Tree.propTypes = {
  childrenKey: PropTypes.string.isRequired,
  formPayload: PropTypes.object,
  onToggleRenderNestedTable: PropTypes.func.isRequired,
  renderNestedBranch: PropTypes.bool,
}

Tree.defaultProps = {
  formPayload: {},
  renderNestedBranch: false,
}

export default Tree
