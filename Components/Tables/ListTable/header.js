import React, { useRef } from 'react'
import PropTypes from 'prop-types'
import { FixedRowsContainer, HeaderContainer } from './styles'
import HeaderCell from './HeaderCell'

const Header = React.forwardRef(
  (
    {
      headerCellComponent,
      onResize,
      onMove,
      openContextMenu,
      columnState: {
        normalColumnsState: { styles, columns },
        fixedColumnsState: { styles: fStyles, columns: fColumns },
      },
    },
    ref,
  ) => {
    const refFixedContainer = useRef()

    return (
      <HeaderContainer className="overflow-hidden relative flex" ref={ref}>
        <FixedRowsContainer style={fStyles} ref={refFixedContainer}>
          {fColumns.map((columnSettings, index) => {
            const { id, label, headerCellComponent: CellComponent = headerCellComponent } = columnSettings

            return (
              <CellComponent
                key={id}
                label={label}
                onResize={onResize(true)(columnSettings, index)}
                onContextMenu={openContextMenu(true)(columnSettings, index)}
                onMove={onMove(true)(id, index)}
              />
            )
          })}
        </FixedRowsContainer>
        <div className="grid row-scrollable-container overflow-hidden" style={styles}>
          {columns.map((columnSettings, index) => {
            const { id, label, headerCellComponent: CellComponent = headerCellComponent } = columnSettings

            return (
              <CellComponent
                key={id}
                label={label}
                id={id}
                onContextMenu={openContextMenu(false)(columnSettings, index)}
                onResize={onResize(false)(columnSettings, index)}
                onMove={onMove(false)(columnSettings, index)}
              />
            )
          })}
        </div>
      </HeaderContainer>
    )
  },
)

Header.propTypes = {
  onResize: PropTypes.func.isRequired,
  onMove: PropTypes.func.isRequired,
  openContextMenu: PropTypes.func.isRequired,
  columnState: PropTypes.shape({
    normalColumnsState: PropTypes.shape({
      styles: PropTypes.object,
      columns: PropTypes.array,
    }),
    fixedColumnsState: PropTypes.shape({
      styles: PropTypes.object,
      columns: PropTypes.array,
    }),
  }).isRequired,
  headerCellComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
}

Header.defaultProps = {
  headerCellComponent: HeaderCell,
}

export default Header
