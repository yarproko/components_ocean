import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import PerfectScrollbar from 'react-perfect-scrollbar'
import PureUpdateArrayItems from '../../../Utils/Arrays/PureUpdateArrayItems'
import PureDeleteItems from '../../../Utils/Arrays/PureDeleteItems'
import { cachedLocalStorageValue } from '../../../Logic/Storages/localStorageCache'
import Row from './row'
import Header from './header'
import {
  ColumnManipulationIndicator,
  ColumnManipulationPositionIndicator,
  ContextMenuButton,
  ContextMenuContainer,
  FakeColumn,
  FakeRowScroll,
  TableBody,
  TableContainer,
} from './styles'
import { useRecoilState } from 'recoil'
import ApplyPlugins from '../Plugins'
import useAutoCloseFrame from '../../../Utils/Hooks/useAutoCloseFrame'

let scrollInterval

const defaultState = { selected: { fixed: true } }

const ListTable = ({
  value,
  renderValue = value,
  onChange,
  onInput,
  getRowPhysicalIndex,
  className,
  headerCellComponent,
  columns,
  id,
  rowComponent,
}) => {
  const [resizeState, setResizeState] = useState({})
  const [contextMenu, setContextMenu] = useState(null)
  const { left, width, columnPositionIndicator, nextIndex, index } = resizeState
  const [columnState, setColumnState] = useRecoilState(cachedLocalStorageValue(`ListTable${id}`))
  const scrollBarRef = useRef()
  const rowsContainerRef = useRef()
  const headerContainerRef = useRef()
  const valueRef = useRef(value)
  valueRef.current = value
  const scrollRef = useRef(0)

  const closeContextMenu = useCallback(() => {
    setContextMenu(null)
  }, [])

  const eventInterceptor = useAutoCloseFrame(closeContextMenu)

  const columnsWithUiSettings = useMemo(() => {
    const state = columnState || defaultState
    const { f, c } = columns.reduce(
      (acc, column) => {
        const { id, sizes = 150 } = column
        const { [id]: { fixed, width = sizes, hidden, fixedPosition, position } = {} } = state
        if (!hidden) {
          if (fixed) {
            acc.f.push({
              columnWidth: width,
              position: fixedPosition,
              column: { ...column, sizes: width },
            })
          } else {
            acc.c.push({
              columnWidth: width,
              position,
              column: { ...column, sizes: width },
            })
          }
        }
        return acc
      },
      { f: [], c: [] },
    )

    const placeColumns = c => {
      c.sort((a, b) => (b.position !== undefined && a.position !== undefined ? (b.position > a.position ? -1 : 1) : 0))
      return c.reduce(
        (acc, { columnWidth, column }) => {
          acc.styles.gridTemplateColumns = `${acc.styles.gridTemplateColumns} ${columnWidth}px`.trim()
          acc.columns.push(column)
          return acc
        },
        {
          styles: { gridTemplateColumns: '' },
          columns: [],
        },
      )
    }

    return {
      fixedColumnsState: placeColumns(f),
      normalColumnsState: placeColumns(c),
    }
  }, [columnState, columns])

  const refColumnsState = useRef(columnsWithUiSettings)
  refColumnsState.current = columnsWithUiSettings

  const onColumnMoving = useCallback(({ clientX }) => {
    clearInterval(scrollInterval)
    const {
      normalColumnsState: { columns },
    } = refColumnsState.current
    setResizeState(prevState => {
      const { right: containerRight, left: containerLeft } = rowsContainerRef.current.getBoundingClientRect()
      const nextLeft = prevState.initialLeft - prevState.initPointerPosition + clientX
      const maxLeft = containerRight - containerLeft - prevState.width
      if (15 + prevState.fixedColumnsWidth > nextLeft) {
        clearInterval(scrollInterval)
        scrollInterval = setInterval(() => {
          scrollBarRef.current._container.scrollLeft -= 5
        }, 5)
      }
      if (containerRight - 15 - containerLeft < nextLeft + prevState.width) {
        clearInterval(scrollInterval)
        scrollInterval = setInterval(() => {
          scrollBarRef.current._container.scrollLeft += 5
        }, 5)
      }

      let i = 0
      let columnPositionIndicator = prevState.fixedColumnsWidth - scrollRef.current
      for (i; i < columns.length; i++) {
        columnPositionIndicator += columns[i].sizes
        if (columnPositionIndicator >= clientX - containerLeft) {
          break
        }
      }

      columnPositionIndicator =
        i <= prevState.index ? columnPositionIndicator - columns[i].sizes : columnPositionIndicator
      return {
        ...prevState,
        left:
          nextLeft > prevState.fixedColumnsWidth
            ? nextLeft < maxLeft
              ? nextLeft
              : maxLeft
            : prevState.fixedColumnsWidth,
        columnPositionIndicator:
          columnPositionIndicator < prevState.fixedColumnsWidth ? undefined : columnPositionIndicator,
        nextIndex: i,
        clientX,
      }
    })
  }, [])

  const onFixedColumnMoving = useCallback(({ clientX }) => {
    setResizeState(prevState => {
      const { left: containerLeft } = rowsContainerRef.current.getBoundingClientRect()
      const {
        fixedColumnsState: { columns },
      } = refColumnsState.current
      const nextLeft = prevState.initialLeft - prevState.initPointerPosition + clientX
      const maxLeft = columns.reduce((acc, i) => acc + i.sizes, 0) - prevState.width
      let i = 0
      let columnPositionIndicator = 0
      for (i; i < columns.length; i++) {
        columnPositionIndicator += columns[i].sizes
        if (columnPositionIndicator >= clientX - containerLeft) {
          break
        }
      }
      columnPositionIndicator =
        i <= prevState.index ? columnPositionIndicator - columns[i].sizes : columnPositionIndicator
      return {
        ...prevState,
        left: nextLeft > 0 ? (nextLeft < maxLeft ? nextLeft : maxLeft) : 0,
        columnPositionIndicator: columnPositionIndicator,
        nextIndex: i,
        clientX,
      }
    })
  }, [])

  const onColumnMoveEnd = useCallback(() => {
    clearInterval(scrollInterval)
    document.removeEventListener('mouseup', onColumnMoveEnd)
    document.body.style.userSelect = ''
    let state
    setResizeState(s => {
      const { listener, index, nextIndex, onMouseMoveSubscriber, fixed } = s
      if (listener) {
        scrollBarRef.current._container.removeEventListener('ps-scroll-x', listener)
      }
      state = { index, nextIndex, fixed }
      document.removeEventListener('mousemove', onMouseMoveSubscriber)
      return {}
    })

    const { index, nextIndex, fixed } = state
    if (index !== nextIndex) {
      const { stateKey, positionKey } = fixed
        ? { positionKey: 'fixedPosition', stateKey: 'fixedColumnsState' }
        : { positionKey: 'position', stateKey: 'normalColumnsState' }
      const {
        [stateKey]: { columns },
      } = refColumnsState.current
      const restState = columnState || defaultState
      const sortColumns = [...columns]
      const [element] = sortColumns.splice(index, 1)
      sortColumns.splice(nextIndex, 0, element)
      setColumnState(
        sortColumns.reduce(
          (acc, { id }, index) => {
            acc[id] = { ...acc[id], [positionKey]: index }
            return acc
          },
          { ...restState },
        ),
      )
    }
  }, [columnState, setColumnState])

  const onColumnStartMove = useCallback(
    fixed =>
      ({ id, label }, index) =>
      e => {
        if (e.nativeEvent.button === 0) {
          const { fixedColumnsState, normalColumnsState } = refColumnsState.current
          if (fixed) {
            const left = fixedColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
            const width = fixedColumnsState.columns[index].sizes
            setResizeState({
              id,
              label,
              index,
              left,
              initialLeft: left,
              width,
              fixed,
              columnPositionIndicator: left,
              clientX: e.clientX,
              initialWidth: width,
              initPointerPosition: e.clientX,
              onMouseMoveSubscriber: onFixedColumnMoving,
            })
            document.addEventListener('mousemove', onFixedColumnMoving)
          } else {
            const fixedColumnsWidth = fixedColumnsState.columns.reduce((acc, i) => acc + i.sizes, 0)
            const selectedOffset = normalColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
            const width = normalColumnsState.columns[index].sizes
            const left = selectedOffset + fixedColumnsWidth - scrollRef.current

            const listener = ({ target: { scrollLeft } }) => {
              setResizeState(prevState => {
                const {
                  normalColumnsState: { columns },
                } = refColumnsState.current
                const { left: containerLeft } = rowsContainerRef.current.getBoundingClientRect()
                let i = 0
                let columnPositionIndicator = prevState.fixedColumnsWidth - scrollLeft
                for (i; i < columns.length; i++) {
                  columnPositionIndicator += columns[i].sizes
                  if (columnPositionIndicator >= prevState.clientX - containerLeft) {
                    break
                  }
                }
                columnPositionIndicator =
                  i <= prevState.index ? columnPositionIndicator - columns[i].sizes : columnPositionIndicator

                return {
                  ...prevState,
                  columnPositionIndicator:
                    columnPositionIndicator < prevState.fixedColumnsWidth ? undefined : columnPositionIndicator,
                  nextIndex: i,
                }
              })
            }
            scrollBarRef.current._container.addEventListener('ps-scroll-x', listener)
            setResizeState({
              id,
              label,
              index,
              initScroll: scrollRef.current,
              initialLeft: left,
              left: left,
              width,
              initialWidth: width,
              initPointerPosition: e.clientX,
              clientX: e.clientX,
              fixedColumnsWidth,
              columnPositionIndicator: left,
              nextIndex: index,
              onMouseMoveSubscriber: onColumnMoving,
              listener,
            })
            document.addEventListener('mousemove', onColumnMoving)
          }
          document.addEventListener('mouseup', onColumnMoveEnd)
          document.body.style.userSelect = 'none'
        }
      },
    [onColumnMoveEnd, onColumnMoving, onFixedColumnMoving],
  )

  const onColumnResizing = useCallback(({ clientX }) => {
    clearInterval(scrollInterval)
    setResizeState(prevState => {
      const nextWidth = prevState.initialWidth - prevState.initPointerPosition + clientX
      const { right: containerRight, left: containerLeft } = rowsContainerRef.current.getBoundingClientRect()
      if (containerLeft + 15 + prevState.fixedColumnsWidth > clientX) {
        clearInterval(scrollInterval)
        scrollInterval = setInterval(() => {
          scrollBarRef.current._container.scrollLeft -= 5
        }, 5)
      }
      if (containerRight - 15 < clientX) {
        clearInterval(scrollInterval)
        scrollInterval = setInterval(() => {
          scrollBarRef.current._container.scrollLeft += 5
        }, 5)
      }
      return {
        ...prevState,
        width: 30 < nextWidth ? nextWidth : 30,
      }
    })
  }, [])

  const onFixedColumnResizing = useCallback(({ clientX }) => {
    const { right: containerRight, left: containerLeft } = rowsContainerRef.current.getBoundingClientRect()
    const {
      fixedColumnsState: { columns },
    } = refColumnsState.current
    setResizeState(prevState => {
      let nextWidth = prevState.initialWidth - prevState.initPointerPosition + clientX
      nextWidth = 30 < nextWidth ? nextWidth : 30
      const maxWidth =
        (containerRight - containerLeft) * 0.8 -
        prevState.left -
        columns.slice(prevState.index + 1).reduce((acc, i) => acc + i.sizes, 0)
      nextWidth = nextWidth > maxWidth ? maxWidth : nextWidth
      return {
        ...prevState,
        width: nextWidth,
      }
    })
  }, [])

  const onColumnStopResize = useCallback(() => {
    document.body.style.cursor = ''
    document.body.style.userSelect = ''
    clearInterval(scrollInterval)
    let state
    setResizeState(({ width, id, listener, onMouseMoveSubscriber }) => {
      if (listener) {
        scrollBarRef.current._container.removeEventListener('ps-scroll-x', listener)
      }
      state = { width, id }
      document.removeEventListener('mousemove', onMouseMoveSubscriber)
      return {}
    })
    document.removeEventListener('mouseup', onColumnStopResize)
    const { width, id } = state
    const { [id]: changedColumn, ...restState } = columnState || defaultState
    setColumnState({
      ...restState,
      [id]: { ...changedColumn, width },
    })
  }, [setColumnState, columnState])

  const onColumnStartResize = useCallback(
    fixed =>
      ({ id, label }, index) =>
      e => {
        e.preventDefault()
        e.stopPropagation()
        const { fixedColumnsState, normalColumnsState } = refColumnsState.current
        if (fixed) {
          const left = fixedColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
          const width = fixedColumnsState.columns[index].sizes
          setResizeState({
            id,
            label,
            index,
            left,
            width,
            initialWidth: width,
            initPointerPosition: e.clientX,
            onMouseMoveSubscriber: onFixedColumnResizing,
          })
          document.addEventListener('mousemove', onFixedColumnResizing)
        } else {
          const fixedColumnsWidth = fixedColumnsState.columns.reduce((acc, i) => acc + i.sizes, 0)
          const selectedOffset = normalColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
          const width = normalColumnsState.columns[index].sizes
          const left = selectedOffset + fixedColumnsWidth

          const listener = ({ target: { scrollLeft } }) => {
            setResizeState(prevState => {
              if (prevState.initialLeft) {
                const nextLeft = prevState.initialLeft - scrollLeft
                return {
                  ...prevState,
                  left: nextLeft > prevState.fixedColumnsWidth ? nextLeft : prevState.fixedColumnsWidth,
                }
              } else {
                return {}
              }
            })
          }
          scrollBarRef.current._container.addEventListener('ps-scroll-x', listener)

          setResizeState({
            id,
            label,
            index,
            initScroll: scrollRef.current,
            initialLeft: left,
            left: left - scrollRef.current,
            width,
            initialWidth: width,
            initPointerPosition: e.clientX,
            fixedColumnsWidth,
            onMouseMoveSubscriber: onColumnResizing,
            listener,
          })
          document.addEventListener('mousemove', onColumnResizing)
        }
        document.addEventListener('mouseup', onColumnStopResize)
        document.body.style.cursor = 'e-resize'
        document.body.style.userSelect = 'none'
      },
    [onColumnResizing, onColumnStopResize, onFixedColumnResizing],
  )

  const openContextMenu = useCallback(
    fixed =>
      ({ id }, index) =>
      e => {
        e.preventDefault()
        const { top: containerTop } = headerContainerRef.current.getBoundingClientRect()
        const { fixedColumnsState, normalColumnsState } = refColumnsState.current
        let config = {}
        if (fixed) {
          config.left = fixedColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
          config.width = fixedColumnsState.columns[index].sizes
          config.fixedStateHandler = () => {
            closeContextMenu()
            const restState = columnState || defaultState
            setColumnState({
              ...restState,
              [id]: { ...restState[id], fixed: false },
            })
          }
        } else {
          const fixedColumnsWidth = fixedColumnsState.columns.reduce((acc, i) => acc + i.sizes, 0)
          const selectedOffset = normalColumnsState.columns.slice(0, index).reduce((acc, i) => acc + i.sizes, 0)
          config.width = normalColumnsState.columns[index].sizes
          config.left = selectedOffset + fixedColumnsWidth
          config.fixedStateHandler = () => {
            closeContextMenu()
            const restState = columnState || defaultState
            setColumnState({
              ...restState,
              [id]: { ...restState[id], fixed: true },
            })
          }
        }
        setContextMenu({
          top: e.clientY - containerTop,
          fixed,
          id,
          index,
          ...config,
        })
      },
    [closeContextMenu, columnState, setColumnState],
  )

  useEffect(() => {
    scrollBarRef.current._container.addEventListener('ps-scroll-x', ({ target: { scrollLeft } }) => {
      for (const element of document.querySelectorAll('.row-scrollable-container')) {
        element.scrollLeft = scrollLeft
      }
      scrollRef.current = scrollLeft
    })
  }, [])

  const onDelete = useCallback(
    rowIndex => {
      onInput(PureDeleteItems(valueRef.current, getRowPhysicalIndex(rowIndex)))
      onChange()
    },
    [getRowPhysicalIndex, onChange, onInput],
  )

  const handleInput = useCallback(
    ({ newData, rowIndex, data }) => {
      onInput(PureUpdateArrayItems(valueRef.current, getRowPhysicalIndex(rowIndex), newData), {
        changedData: { [rowIndex]: data },
      })
      onChange()
    },
    [getRowPhysicalIndex, onChange, onInput],
  )

  const getParentValue = useCallback(
    (acc = []) => {
      acc.unshift(value)
      return acc
    },
    [value],
  )

  return (
    <TableContainer className={`${className} flex h-full flex-col relative overflow-hidden`}>
      {contextMenu && (
        <ContextMenuContainer
          onMouseDown={eventInterceptor}
          width={contextMenu.width}
          left={contextMenu.left}
          top={contextMenu.top}
        >
          <ContextMenuButton type="button" onClick={contextMenu.fixedStateHandler}>
            {contextMenu.fixed ? 'Открепить' : 'закрепить'}
          </ContextMenuButton>
        </ContextMenuContainer>
      )}
      <TableBody className="flex h-full flex-col" ref={rowsContainerRef}>
        <Header
          ref={headerContainerRef}
          headerCellComponent={headerCellComponent}
          onResize={onColumnStartResize}
          onMove={onColumnStartMove}
          openContextMenu={openContextMenu}
          columnState={columnsWithUiSettings}
        />
        <PerfectScrollbar className="h-full pb-2.5">
          {renderValue.map((v, i) => (
            <Row
              key={i}
              rowIndex={i}
              value={v}
              rowComponent={rowComponent}
              getParentValue={getParentValue}
              columnState={columnsWithUiSettings}
              onInput={handleInput}
              onDelete={onDelete}
            />
          ))}
          <ColumnManipulationIndicator left={left} width={width} className="absolute top-0 bottom-0" />
          <ColumnManipulationPositionIndicator
            columnPositionIndicator={columnPositionIndicator}
            nextIndex={nextIndex}
            index={index}
            className="bg-red-900 absolute top-0 bottom-0"
          />
        </PerfectScrollbar>
        <FakeRowScroll ref={scrollBarRef}>
          <div className="flex items-center">
            <div className="grid" style={columnsWithUiSettings.fixedColumnsState.styles} />
            <div className="grid" style={columnsWithUiSettings.normalColumnsState.styles}>
              {columnsWithUiSettings.normalColumnsState.columns.map((v, i) => (
                <FakeColumn key={i} />
              ))}
            </div>
          </div>
        </FakeRowScroll>
      </TableBody>
    </TableContainer>
  )
}

ListTable.propTypes = {
  headerCellComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  onInput: PropTypes.func,
  getRowPhysicalIndex: PropTypes.func,
  value: PropTypes.array,
  parentCollapsedColumnState: PropTypes.object,
  columns: PropTypes.array,
  initRowIndex: PropTypes.number,
  loading: PropTypes.bool,
  renderValue: PropTypes.array,
  className: PropTypes.string,
  onChange: PropTypes.func,
  rowComponent: PropTypes.func,
  id: PropTypes.string.isRequired,
}
ListTable.defaultProps = {
  value: [],
  headerCellComponent: undefined, // определенно в Header
  rowComponent: undefined, // определенно в Row
  renderValue: undefined, // value by default
  initRowIndex: 0,
  parentCollapsedColumnState: {},
  getRowPhysicalIndex: i => i,
  onInput: () => null,
  className: '',
  onChange: () => null,
  columns: '',
  loading: false,
}

export default ApplyPlugins(ListTable)
