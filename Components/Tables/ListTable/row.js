import PropTypes from 'prop-types'
import AccumulateFunctionCall from '../../../Utils/FunctionCall/AccumulateFunctionCall'
import Cell from './Cell'
import { CellContainer } from './styles'
import DefaultRowComponent from './DefaultRowComponent'
import { useCallback, useMemo } from 'react'

const Row = ({
  value,
  rowIndex,
  columnState,
  onInput,
  getParentValue,
  columnState: {
    normalColumnsState: { styles, columns },
    fixedColumnsState: { styles: fStyles, columns: fColumns },
  },
  rowComponent: RowComponent,
}) => {
  const handleInput = useMemo(
    () =>
      AccumulateFunctionCall((...updates) => {
        const { value, rowIndex, onInput } = this.props
        onInput(
          updates.reduce(
            (acc, [value, id]) => {
              if (value === undefined) {
                delete acc.newData[id]
              } else {
                acc.newData[id] = value
              }
              acc.data[id] = value
              return acc
            },
            { newData: { ...value }, data: {}, rowIndex },
          ),
        )
      }),
    [],
  )

  const handleDelete = useCallback(() => {
    const { onDelete, rowIndex } = this.props
    onDelete(rowIndex)
  }, [])

  return (
    <RowComponent
      value={value}
      rowComponent={RowComponent}
      columnState={columnState}
      onInput={onInput}
      id={rowIndex}
      getParentValue={getParentValue}
      className="flex py-2 relative overflow-x-hidden"
    >
      <div className="grid" style={fStyles}>
        {fColumns.map(({ component: CellComponent = Cell, style, id, props, className = '' }) => (
          <CellContainer key={id} className={`${className} flex items-baseline`} style={style}>
            <CellComponent
              {...props}
              id={id}
              value={value[id]}
              rowIndex={rowIndex}
              ParentValue={value}
              getParentValue={getParentValue}
              onInput={handleInput}
              onDelete={handleDelete}
            />
          </CellContainer>
        ))}
      </div>
      <div className="grid row-scrollable-container overflow-hidden" style={styles}>
        {columns.map(({ component: CellComponent = Cell, style, id, props, className = '' }) => (
          <CellContainer key={id} className={`${className} flex items-baseline`} style={style}>
            <CellComponent
              {...props}
              id={id}
              value={value[id]}
              rowIndex={rowIndex}
              ParentValue={value}
              getParentValue={getParentValue}
              onInput={handleInput}
              onDelete={handleDelete}
            />
          </CellContainer>
        ))}
      </div>
    </RowComponent>
  )
}

Row.propTypes = {
  columnState: PropTypes.shape({
    normalColumnsState: PropTypes.shape({
      styles: PropTypes.object,
      columns: PropTypes.array,
    }),
    fixedColumnsState: PropTypes.shape({
      styles: PropTypes.object,
      columns: PropTypes.array,
    }),
  }).isRequired,
  onInput: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  rowIndex: PropTypes.number.isRequired,
  getParentValue: PropTypes.func.isRequired,
  value: PropTypes.object,
  rowComponent: PropTypes.func,
}
Row.defaultProps = {
  value: {},
  rowComponent: DefaultRowComponent,
}

export default Row
