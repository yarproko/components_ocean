import styled, { css } from 'styled-components'
import PerfectScrollbar from 'react-perfect-scrollbar'

export const CellContainer = styled.div`
  display: flex;
  padding-top: var(--rowPadding);
  padding-bottom: var(--rowPadding);
`

export const TableContainer = styled.div`
  --separator-width: 2px;
  .ps__rail-y {
    z-index: 2;
  }
`
export const TableBody = styled.div`
  --separator-width: 1px;
`
export const HeaderContainer = styled.div`
  flex: 0 0 auto;
  border-bottom: 1px solid var(--separator-color, #bdbdbd);
`

export const GridContainer = styled.div`
  border-bottom: 1px solid var(--separator-color, #bdbdbd);
`

export const FixedRowsContainer = styled.div`
  display: grid;
`

export const FakeRowScroll = styled(PerfectScrollbar)`
  height: 15px;
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  ${TableBody}:hover & {
    .ps__rail-x {
      opacity: 0.9;
    }
  }
`

export const FakeColumn = styled.div`
  height: 1px;
`

export const ColumnManipulationIndicator = styled.div`
  background: var(--elements-hover-color, #bfa764);
  ${({ left, width }) =>
    left !== undefined
      ? css`
          left: ${left}px;
          width: ${width}px;
        `
      : 'display: none;'}
  opacity: 0.4;
`
export const ColumnManipulationPositionIndicator = styled.div`
  background: var(--elements-active-color, #a18946);
  ${({ columnPositionIndicator, index, nextIndex }) =>
    columnPositionIndicator !== undefined
      ? `left: calc(${columnPositionIndicator}px ${index > nextIndex ? '+ 0px' : '- 4px'});`
      : 'display: none;'}
  width: 4px;
`

export const ContextMenuContainer = styled.div`
  position: absolute;
  box-shadow: 0 8px 16px rgba(0, 0, 0, 0.15);
  background: white;
  border-radius: 8px;
  z-index: 1000;
  overflow: hidden;
  ${({ width, left, top }) => css`
    width: ${width}px;
    left: ${left}px;
    top: ${top}px;
  `}
`

export const ContextMenuButton = styled.button`
  padding: 0.5rem 0.75rem;
  transition: 250ms ease-in-out all;
  width: 100%;
  text-align: left;
  &:hover {
    background-color: var(--elements-hover-color, #bfa764);
    color: white;
  }
`
