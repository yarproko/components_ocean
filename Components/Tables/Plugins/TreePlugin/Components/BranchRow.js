import { useCallback, useContext } from 'react'
import PropTypes from 'prop-types'
import { TreeStateContext, TreeStateLevelContext } from '../../constants'
import PureDeleteItems from '../../../../../Utils/Arrays/PureDeleteItems'
import Row from '../../../ListTable/row'
import PureUpdateArrayItems from '../../../../../Utils/Arrays/PureUpdateArrayItems'

const BranchRow = Comp => {
  const NestedRowContainer = props => {
    const { value, rowComponent, onInput, id, columnState, getParentValue } = props
    const {
      nestedDataKey,
      valueKey,
      defaultExpandAll,
      state: { [value[valueKey]]: expanded = defaultExpandAll },
    } = useContext(TreeStateContext)

    const subRow = useContext(TreeStateLevelContext)

    const onNestedTableInput = useCallback(
      ({ newData, rowIndex, data }) => {
        onInput({
          newData: {
            ...value,
            [nestedDataKey]: PureUpdateArrayItems(value[nestedDataKey], rowIndex, newData),
          },
          rowIndex: id,
          data: { [rowIndex]: data },
        })
      },
      [id, nestedDataKey, onInput, value],
    )

    const onDelete = useCallback(
      rowIndex => {
        onInput(PureDeleteItems(value, rowIndex))
      },
      [onInput, value],
    )

    const handleGetParentValue = useCallback(
      (acc = []) => {
        acc.unshift(value)
        return getParentValue(acc)
      },
      [getParentValue, value],
    )

    return (
      <div className="flex flex-col">
        <Comp {...props} className={expanded ? `${props.className} sticky bg-white` : props.className} />
        <TreeStateLevelContext.Provider value={subRow + 1}>
          {expanded &&
            value[nestedDataKey] &&
            value[nestedDataKey].map((v, i) => (
              <Row
                key={i}
                rowIndex={i}
                value={v}
                getParentValue={handleGetParentValue}
                columnState={columnState}
                rowComponent={rowComponent}
                onInput={onNestedTableInput}
                onDelete={onDelete}
              />
            ))}
        </TreeStateLevelContext.Provider>
      </div>
    )
  }
  NestedRowContainer.propTypes = {
    rowComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
    onInput: PropTypes.func.isRequired,
    getParentValue: PropTypes.func.isRequired,
    columnState: PropTypes.object.isRequired,
    value: PropTypes.array,
    className: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }

  NestedRowContainer.defaultProps = {
    className: '',
    value: [],
  }

  return NestedRowContainer
}

export default BranchRow
