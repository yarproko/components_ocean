import { useContext } from 'react'
import PropTypes from 'prop-types'
import CircleMinus from '../../../../Tree/Icons/CircleMinus'
import CirclePlus from '../../../../Tree/Icons/CirclePlus'
import Dot from '../../../../Tree/Icons/Dot'
import { TreeStateContext, TreeStateLevelContext } from '../../constants'
import styled from 'styled-components'

const LeafContainer = styled.div`
  padding-left: ${({ subRow }) => subRow * 4}px;
`

const Leaf = ({ ParentValue, children, className, ChildrenLessIcon, OpenIcon, CloseIcon }) => {
  const {
    valueKey,
    defaultExpandAll,
    nestedDataKey,
    state: { [ParentValue[valueKey]]: expanded = defaultExpandAll },
    onChange,
  } = useContext(TreeStateContext)

  const subRow = useContext(TreeStateLevelContext)

  const MainIcon = expanded ? CloseIcon : OpenIcon
  return (
    <LeafContainer subRow={subRow} className={`${className} flex items-center`}>
      {ParentValue[nestedDataKey] && ParentValue[nestedDataKey].length > 0 ? (
        <MainIcon onClick={onChange(ParentValue[valueKey])} className="mr-1" />
      ) : (
        <ChildrenLessIcon className="mr-1" />
      )}
      {children}
    </LeafContainer>
  )
}

Leaf.propTypes = {
  ParentValue: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  className: PropTypes.string,
  ChildrenLessIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  OpenIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  CloseIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
}

Leaf.defaultProps = {
  className: '',
  ChildrenLessIcon: Dot,
  OpenIcon: CirclePlus,
  CloseIcon: CircleMinus,
  ParentValue: {},
  children: null,
}

export default Leaf
