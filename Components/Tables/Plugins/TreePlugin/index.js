import { useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import Leaf from './Components/Leaf'
import Cell from '../../ListTable/Cell'
import DefaultRowComponent from '../../ListTable/DefaultRowComponent'

import { TreeStateContext } from '../constants'
import BranchRow from './Components/BranchRow'

const TreePlugin = Component => {
  const Tree = ({ columns, plugins, rowComponent, ...props }) => {
    const [leafStates, setLeaf] = useState({})
    const [{ component: Comp = Cell, ...columnSettings }] = columns
    const {
      treePlugin,
      treePlugin: { component: LeafComponent = Leaf, valueKey, nestedDataKey, defaultOpen },
    } = plugins
    return (
      <TreeStateContext.Provider
        value={useMemo(
          () => ({
            nestedDataKey,
            defaultOpen,
            valueKey,
            state: leafStates,
            onChange: id => () => setLeaf(prevState => ({ ...prevState, [id]: !prevState[id] })),
          }),
          [leafStates, treePlugin],
        )}
      >
        <Component
          {...props}
          {...useMemo(() => {
            return {
              columns: [
                {
                  ...columnSettings,
                  component: props => (
                    <LeafComponent {...props}>
                      <Comp {...props} />
                    </LeafComponent>
                  ),
                },
                ...columns.slice(1),
              ],
              rowComponent: BranchRow(rowComponent),
            }
          }, [rowComponent, columns])}
          plugins={plugins}
        />
      </TreeStateContext.Provider>
    )
  }

  Tree.propTypes = {
    plugins: PropTypes.object,
    columns: PropTypes.array,
    rowComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  }

  Tree.defaultProps = {
    plugins: {},
    columns: [],
    rowComponent: DefaultRowComponent,
  }

  return Tree
}

export default TreePlugin
