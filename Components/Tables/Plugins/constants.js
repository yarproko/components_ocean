import React from 'react'
export const SelectStateContext = React.createContext({ state: [], onChange: () => null, value: [], valueKey: '' })
export const SortStateContext = React.createContext({ state: {}, onChange: () => null })
export const TreeStateContext = React.createContext({
  state: {},
  onChange: () => undefined,
  nestedDataKey: 'children',
  valueKey: 'ID',
  defaultExpandAll: false,
})

export const TreeStateLevelContext = React.createContext(0)
export const FilterStateContext = React.createContext({ state: {}, onChange: () => null })
