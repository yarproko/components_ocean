import { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Icon from '../../../../Icon'
import { FilterStateContext } from '../../constants'
import FilterIcon from '../../../../../Icons/filterIcon'
import CheckMark from '../../../../../Icons/checkMarkIcon'
import { CheckMarkIcon, GoldIcon } from './styles'

const SortButton = styled.button`
  user-select: none;
  transition: color 250ms ease-in-out;
  color: ${({ current }) => (current ? 'var(--buttons-active-color, #BFA764)' : 'var(--buttons-base-color, #C4C4C4)')};

  &:hover {
    color: var(--buttons-hover-color, #bfa764);
  }
`

const DefaultFilterCell = Component => {
  const Cell = ({ className, style, id, ...props }) => {
    const {
      state: { key, state },
      onChange,
    } = useContext(FilterStateContext)
    return (
      <div className={`${className} flex items-center h-full w-full`} style={style}>
        <Component id={id} {...props} />
        <div className="flex flex-col mr-1.5">
          <SortButton type="button" onClick={onChange(id)}>
            {key === id && state !== undefined ? (
              <div className="relative">
                <GoldIcon icon={FilterIcon} size={16} />
                <CheckMarkIcon icon={CheckMark} size={9} />
              </div>
            ) : (
              <Icon icon={FilterIcon} size={16} />
            )}
          </SortButton>
        </div>
      </div>
    )
  }

  Cell.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }

  Cell.defaultProps = {
    className: '',
    style: undefined,
  }

  return Cell
}

export default DefaultFilterCell
