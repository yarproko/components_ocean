import { useState } from 'react'
import PropTypes from 'prop-types'

import ContextMenu from '../../../../ContextMenu'
import CheckBoxGroupInput from '../../../../Inputs/CheckboxGroup'
import { FilterFormContainer, FilterSubmitButton } from './styles'
import { ReverseCheckBox } from '../../../../Inputs/CheckBox'

const InnerFilterFormContainer = ({ value, columnId, state, applyFilter, target, onClose }) => {
  const [tempState, setTempState] = useState(state)

  return (
    <ContextMenu onClose={onClose} target={target}>
      <FilterFormContainer>
        <CheckBoxGroupInput
          id="filter"
          options={value}
          labelKey={columnId}
          valueKey={columnId}
          value={tempState}
          onInput={setTempState}
          title={`Фильтр колонки ${columnId}`}
          placeholder="текст"
          checkBoxComponent={ReverseCheckBox}
        />
        <FilterSubmitButton
          className="ml-auto"
          onClick={() => {
            applyFilter(tempState)
            onClose()
          }}
        >
          Применить
        </FilterSubmitButton>
      </FilterFormContainer>
    </ContextMenu>
  )
}

InnerFilterFormContainer.propTypes = {
  applyFilter: PropTypes.func.isRequired,
  target: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  value: PropTypes.array,
  columnId: PropTypes.string.isRequired,
  state: PropTypes.array,
}

InnerFilterFormContainer.defaultProps = {
  value: [],
  state: {},
}

export default InnerFilterFormContainer
