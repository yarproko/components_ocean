import styled from 'styled-components'
import Button from '../../../../Button'
import Icon from '../../../../Icon'

export const FilterFormContainer = styled.div`
  background: white;
  width: 250px;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  padding: 1rem;
  display: flex;
  flex-direction: column;
`

export const FilterSubmitButton = styled(Button)`
  background: #bfa764;
  color: white;
  margin-top: 0.5rem;
`

export const CheckMarkIcon = styled(Icon)`
  position: absolute;
  right: -1px;
  top: -1px;
  color: #333333;
`

export const GoldIcon = styled(Icon)`
  color: #bfa764;
`
