import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import DefaultHeaderComponent from '../../Grid/Components/DefaultHeaderComponent'
import HeaderCell from '../../ListTable/HeaderCell'
import InnerFilter from './innerFilter'

const Filter = Component => {
  const WithFilter = React.forwardRef((props, ref) => {
    const {
      columns,
      plugins,
      plugins: {
        filterPlugin: { controller: FilterController = InnerFilter },
      },
      headerCellComponent: CellComponent,
    } = props

    const columnsWithPluginSettings = useMemo(() => {
      const {
        filterPlugin: { component: filterComponent = CellComponent, columnsSettings = {} },
      } = plugins
      return columns.map(({ headerCellComponent = DefaultHeaderComponent, id, ...c }) => {
        const { [id]: { cellComponent: cellSortComponent = filterComponent } = {} } = columnsSettings
        return {
          id,
          ...c,
          headerCellComponent: cellSortComponent(headerCellComponent),
        }
      })
    }, [CellComponent, columns, plugins])

    return (
      <FilterController {...props}>
        {controllerProps => <Component ref={ref} {...props} {...controllerProps} columns={columnsWithPluginSettings} />}
      </FilterController>
    )
  })

  WithFilter.propTypes = {
    value: PropTypes.array,
    columns: PropTypes.array,
    plugins: PropTypes.shape({
      filterPlugin: PropTypes.shape({
        controller: PropTypes.func,
        component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
        columnsSettings: PropTypes.object,
      }).isRequired,
    }),
    headerCellComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  }

  WithFilter.defaultProps = {
    columns: [],
    value: [],
    plugins: {},
    headerCellComponent: HeaderCell,
  }

  return WithFilter
}

export default Filter
