import { useCallback, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { FilterStateContext } from '../constants'
import InnerFilterFormContainer from './Components/InnerFilterFormContainer'

const defaultFilterFunction = (row, key) => s => row[key] !== s

const InnerFilter = ({ value, children, filterFunctions }) => {
  const [filterState, setFilterState] = useState({})
  const [menuEvent, openMenuEvent] = useState(null)

  const applyFilter = useCallback(
    key => state => {
      setFilterState({ key, state })
    },
    [],
  )

  return (
    <FilterStateContext.Provider
      value={useMemo(
        () => ({
          state: filterState,
          onChange: id => e => {
            openMenuEvent({ target: e.target, key: id })
          },
        }),
        [filterState],
      )}
    >
      {menuEvent && (
        <InnerFilterFormContainer
          onClose={() => openMenuEvent(null)}
          target={menuEvent.target}
          value={value}
          columnId={menuEvent.key}
          state={menuEvent.key === filterState.key ? filterState.state : undefined}
          applyFilter={applyFilter(menuEvent.key)}
        />
      )}
      {children(
        useMemo(() => {
          const { key, state } = filterState
          const { [key]: filterFunction = defaultFilterFunction } = filterFunctions
          if (state) {
            const { renderValue, indexesMap } = value.reduce(
              (acc, value, index) => {
                if (state.every(filterFunction(value, key))) {
                  acc.renderValue.push(value)
                  acc.indexesMap.push(index)
                }
                return acc
              },
              { renderValue: [], indexesMap: [] },
            )
            return {
              renderValue,
              getRowPhysicalIndex: index => indexesMap[index],
            }
          }
          return {
            renderValue: value,
            getRowPhysicalIndex: index => index,
          }
        }, [filterState, filterFunctions, value]),
      )}
    </FilterStateContext.Provider>
  )
}

InnerFilter.propTypes = {
  value: PropTypes.array,
  children: PropTypes.func.isRequired,
  filterFunctions: PropTypes.object,
}

InnerFilter.defaultProps = {
  value: [],
  filterFunctions: {},
}

export default InnerFilter
