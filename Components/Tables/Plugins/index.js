import PropTypes from 'prop-types'
import selectPlugin from './selectable'
import compose from 'lodash/fp/compose'
import { useMemo } from 'react'
import outerSort from './sort/outerSort'
import TreePlugin from './TreePlugin'
import Filter from './filter'

const pluginDictionary = {
  selectPlugin,
  outerSortPlugin: outerSort,
  treePlugin: TreePlugin,
  filterPlugin: Filter,
}

// очередность плагинов важна!
const pluginsList = ['outerSortPlugin', 'filterPlugin', 'treePlugin', 'selectPlugin']

export const ApplyPlugins = Components => {
  const TableWithPlugins = props => {
    const { plugins } = props
    const Comp = useMemo(() => {
      const applyingPlugins = []
      pluginsList.forEach(pluginKey => {
        if (plugins[pluginKey] !== undefined) {
          applyingPlugins.push(pluginDictionary[pluginKey])
        }
      })
      return compose(...applyingPlugins)(Components)
    }, [plugins])

    return <Comp {...props} />
  }

  TableWithPlugins.propTypes = {
    plugins: PropTypes.object,
  }

  TableWithPlugins.defaultProps = {
    plugins: {},
    value: [],
  }

  return TableWithPlugins
}

export default ApplyPlugins
