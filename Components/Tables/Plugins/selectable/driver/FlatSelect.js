import { useCallback, useContext, useMemo } from 'react'
import { SelectStateContext } from '../../constants'
import { TableCheckBox } from './styles'
import PropTypes from 'prop-types'

const WithFlatSelectHeader = Comp => {
  const FlatSelectHeaderCell = props => {
    const { state, onChange, value, valueKey, returnObjects } = useContext(SelectStateContext)
    const { selectValue, sendValue } = useMemo(() => {
      return returnObjects
        ? {
            selectValue: value.length > 0 && value.every(v => state.some(s => s[valueKey] === v[valueKey])),
            sendValue: () => [...new Set([...state, ...value])],
          }
        : {
            selectValue: value.length > 0 && value.every(v => state.includes(v[valueKey])),
            sendValue: () => [...new Set([...state, ...value.map(v => v[valueKey])])],
          }
    }, [returnObjects, valueKey, state, value])

    return (
      <Comp
        {...props}
        value={selectValue}
        onInput={useCallback(v => onChange(v ? sendValue() : []), [onChange, sendValue])}
      />
    )
  }

  FlatSelectHeaderCell.propTypes = {}

  return FlatSelectHeaderCell
}

const WithFlatSelectCell = Comp => {
  const FlatSelectCell = props => {
    const { state, onChange, valueKey, returnObjects } = useContext(SelectStateContext)
    return (
      <Comp
        {...props}
        value={state}
        checkBoxValue={returnObjects ? props.ParentValue : props.ParentValue[valueKey]}
        valueKey={valueKey}
        returnObjects={returnObjects}
        onInput={onChange}
      />
    )
  }

  FlatSelectCell.propTypes = {
    ParentValue: PropTypes.object,
  }

  FlatSelectCell.defaultProps = {
    ParentValue: {},
  }

  return FlatSelectCell
}

const FlatSelect = ({ component: Comp = TableCheckBox, ...settings }) => ({
  ...settings,
  headerCellComponent: WithFlatSelectHeader(Comp),
  component: WithFlatSelectCell(Comp),
})

export default FlatSelect
