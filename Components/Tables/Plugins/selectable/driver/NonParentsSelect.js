import { useContext } from 'react'
import PropTypes from 'prop-types'
import { SelectStateContext, TreeStateContext } from '../../constants'
import { TableCheckBox } from './styles'

const WithNonParentCell = Comp => {
  const NonParentCell = props => {
    const { state, onChange, valueKey, returnObjects } = useContext(SelectStateContext)
    const { nestedDataKey } = useContext(TreeStateContext)
    return (
      props.ParentValue[nestedDataKey] === undefined && (
        <Comp
          {...props}
          value={state}
          checkBoxValue={returnObjects ? props.ParentValue : props.ParentValue[valueKey]}
          valueKey={valueKey}
          returnObjects={returnObjects}
          onInput={onChange}
        />
      )
    )
  }
  NonParentCell.propTypes = {
    ParentValue: PropTypes.object,
  }

  NonParentCell.defaultProps = {
    ParentValue: {},
  }

  return NonParentCell
}

const NonParentSelect = ({ component: Comp = TableCheckBox, ...settings }) => ({
  ...settings,
  component: WithNonParentCell(Comp),
})

export default NonParentSelect
