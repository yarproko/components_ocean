import styled from 'styled-components'
import CheckBox from '../../../../Inputs/CheckBox'

export const TableCheckBox = styled(CheckBox)`
  margin: auto 0;
`
