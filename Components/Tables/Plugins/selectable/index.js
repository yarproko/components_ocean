import { useMemo } from 'react'
import PropTypes from 'prop-types'
import { SelectStateContext } from '../constants'
import FlatSelect from './driver/FlatSelect'
import NonParentsSelect from './driver/NonParentsSelect'

export { FlatSelect, NonParentsSelect }

const SelectablePlugin = Component => {
  const Selectable = ({
    selectState,
    onSelect,
    plugins,
    plugins: {
      selectPlugin: { driver, valueKey, returnObjects, ...columnSettings },
    },
    columns,
    ...props
  }) => {
    return (
      <SelectStateContext.Provider
        value={useMemo(
          () => ({
            state: selectState,
            onChange: onSelect,
            value: props.value,
            valueKey,
            returnObjects,
          }),
          [selectState, onSelect, props.value, valueKey, returnObjects],
        )}
      >
        <Component
          {...props}
          plugins={plugins}
          columns={useMemo(() => {
            return [
              driver({
                id: 'selected',
                sizes: 40,
                ...columnSettings,
              }),
              ...columns,
            ]
          }, [driver, columnSettings, columns])}
        />
      </SelectStateContext.Provider>
    )
  }
  Selectable.propTypes = {
    plugins: PropTypes.object,
    selectState: PropTypes.array,
    columns: PropTypes.array,
    value: PropTypes.array,
    onSelect: PropTypes.func.isRequired,
  }

  Selectable.defaultProps = {
    selectState: [],
    columns: [],
    value: [],
    plugins: {},
  }

  return Selectable
}

export default SelectablePlugin
