import { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import sortAngleIcon from '../../../../../Icons/sortAngleIcon'
import Icon from '../../../../Icon'
import { SortStateContext } from '../../constants'

const SortButton = styled.button`
  user-select: none;
  transition: color 250ms ease-in-out;
  color: ${({ current }) => (current ? 'var(--buttons-active-color, #BFA764)' : 'var(--buttons-base-color, #C4C4C4)')};
  &:hover {
    color: var(--buttons-hover-color, #bfa764);
  }
`

const DefaultSortCell = Component => {
  const Cell = ({ className, style, id, ...props }) => {
    const {
      state: { key, direction },
      onChange,
      upperDirectionKey,
      downDirectionKey,
    } = useContext(SortStateContext)
    return (
      <div className={`${className} flex items-center`} style={style}>
        <div className="flex flex-col mr-1.5">
          <SortButton
            type="button"
            onClick={() => onChange(id, upperDirectionKey)}
            current={direction === upperDirectionKey && id === key}
          >
            <Icon icon={sortAngleIcon} size={8} />
          </SortButton>
          <SortButton
            type="button"
            onClick={() => onChange(id, downDirectionKey)}
            current={direction === downDirectionKey && id === key}
          >
            <Icon icon={sortAngleIcon} size={8} className="rotate-180" />
          </SortButton>
        </div>
        <Component id={id} {...props} />
      </div>
    )
  }

  Cell.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    id: PropTypes.string.isRequired,
  }

  Cell.defaultProps = {
    className: '',
    style: undefined,
  }

  return Cell
}

export default DefaultSortCell
