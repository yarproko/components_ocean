import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { SortStateContext } from '../constants'
import DefaultSortCell from './Components/DefaultSortCell'
import HeaderCell from '../../ListTable/HeaderCell'

const WithValueSort = Component => {
  const WithSort = React.forwardRef(
    (
      {
        sortQuery,
        onSort,
        columns,
        headerCellComponent: CellComponent,
        plugins,
        plugins: {
          outerSortPlugin,
          outerSortPlugin: {
            component: sortComponent = DefaultSortCell,
            columnsSettings = {},
            upperDirectionKey = 'ASC',
            downDirectionKey = 'DSC',
          },
        },
        ...props
      },
      ref,
    ) => {
      return (
        <SortStateContext.Provider
          value={useMemo(
            () => ({
              state: sortQuery,
              upperDirectionKey,
              downDirectionKey,
              onChange: (key, direction) => {
                if (sortQuery.key === key) {
                  onSort(direction === sortQuery.direction ? {} : { key, direction })
                } else {
                  onSort({ key, direction })
                }
              },
            }),
            [outerSortPlugin, onSort, sortQuery],
          )}
        >
          <Component
            {...props}
            ref={ref}
            plugins={plugins}
            columns={useMemo(() => {
              return columns.map(({ headerCellComponent = CellComponent, id, ...c }) => {
                const { [id]: { cellComponent: cellSortComponent = sortComponent } = {} } = columnsSettings
                return {
                  id,
                  ...c,
                  headerCellComponent: cellSortComponent(headerCellComponent),
                }
              })
            }, [CellComponent, columns, sortComponent, columnsSettings])}
          />
        </SortStateContext.Provider>
      )
    },
  )

  WithSort.propTypes = {
    sortQuery: PropTypes.object,
    onSort: PropTypes.func.isRequired,
    headerCellComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    columns: PropTypes.array,
    plugins: PropTypes.shape({
      outerSortPlugin: PropTypes.shape({
        component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
        columnsSettings: PropTypes.object,
        upperDirectionKey: PropTypes.string,
        downDirectionKey: PropTypes.string,
      }),
    }),
  }
  WithSort.defaultProps = {
    plugins: {},
    sortQuery: {},
    columns: [],
    headerCellComponent: HeaderCell,
    onInput: () => null,
  }
  return WithSort
}

export default WithValueSort
