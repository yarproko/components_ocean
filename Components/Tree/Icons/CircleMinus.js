import Icon from '../../Icon'
import CircleMinus from '../../../Icons/CircleMinus'
import PropTypes from 'prop-types'

const CircleMinusIcon = ({ onClick, ...props }) => <Icon icon={CircleMinus} onClick={onClick} size={12} {...props} />

CircleMinusIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default CircleMinusIcon
