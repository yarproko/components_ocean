import Icon from '../../Icon'
import CirclePlus from '../../../Icons/CirclePlus'
import PropTypes from 'prop-types'

const CirclePlusIcon = ({ onClick, ...props }) => <Icon icon={CirclePlus} onClick={onClick} size={12} {...props} />

CirclePlusIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default CirclePlusIcon
