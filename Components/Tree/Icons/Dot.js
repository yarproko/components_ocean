import Icon from '../../Icon'
import Dot from '../../../Icons/Dot'
import PropTypes from 'prop-types'

const DotIcon = ({ onClick, ...props }) => <Icon icon={Dot} onClick={onClick} size={4} {...props} />

DotIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default DotIcon
