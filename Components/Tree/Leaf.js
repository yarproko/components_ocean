import { useCallback, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { ChildrenContainer, HeaderContainer, LeafContainer } from './styles'
import CheckBox from '../Inputs/CheckBox'
import CircleMinusIcon from './Icons/CircleMinus'
import CirclePlusIcon from './Icons/CirclePlus'
import DotIcon from './Icons/Dot'
import Row from './Row'

const Leaf = props => {
  const {
    childrenKey,
    valueKey,
    options,
    labelKey,
    options: { [labelKey]: title, [childrenKey]: children, [valueKey]: leafVal },
    level,
    onInput,
    index,
    defaultExpandAll,
    getLeafSelectedStatus,
    onSelect,
    selectedNode,
    draggable,
    checkAble,
    getSequence,
    dropRule,
    setDropState,
    dropState,
    rowComponent,
    parent,
    onUpdateOptions,
    onDeleteLeafOption,
    OpenIcon,
    CloseIcon,
    ChildrenLessIcon,
    LeafComponent,
    className,
    returnObjects,
  } = props

  const refProps = useRef(props)
  refProps.current = props

  const [borderState, setBorderState] = useState('')
  const [expanded, setExpanded] = useState(defaultExpandAll)

  const toggleOpen = useCallback(() => setExpanded(v => !v), [])

  const checkBoxInput = useCallback(
    value => {
      if (children) {
        const nextValue = []
        const stack = [options]
        if (value) {
          for (let i = 0; i < stack.length; i++) {
            const item = stack[i]

            const { [childrenKey]: stackChildren } = item
            if (stackChildren) {
              stackChildren.forEach(item => {
                stack.push(item)
              })
            } else {
              nextValue.push([item[valueKey], returnObjects ? item : item[valueKey]])
            }
          }
        }
        onInput(nextValue, value)
      } else {
        onInput([[leafVal, returnObjects ? options : leafVal]], value)
      }
    },
    [children, options, onInput, childrenKey, valueKey, returnObjects, leafVal],
  )

  const onDragEnd = useCallback(() => {
    setBorderState('')
    setDropState(null)
  }, [setDropState])

  const onDrop = useCallback(() => {
    setBorderState('')
    setDropState(null)
  }, [setDropState])

  const onDragOver = useCallback(event => {
    event.stopPropagation()
    event.preventDefault()
    const {
      dropRule,
      dropState: { node },
    } = refProps.current
    if (dropRule(node, refProps.current)) {
      const {
        nativeEvent: { y },
        target,
      } = event
      const { y: elementY, top, bottom } = target.getBoundingClientRect()
      const height = bottom - top

      setBorderState(height / (y - elementY) > 2 ? 'top' : 'bottom')
    }
  }, [])

  const onDragLeave = useCallback(() => {
    setBorderState('')
  }, [])

  const handleGetSequence = useCallback(
    (sequence = []) => {
      sequence.unshift(index)
      return getSequence(sequence)
    },
    [getSequence, index],
  )

  const onUpdateLeafOption = useCallback(
    nextLeafValue => {
      onUpdateOptions(nextLeafValue, index)
    },
    [index, onUpdateOptions],
  )

  const handleDeleteLeafOption = useCallback(() => {
    onDeleteLeafOption(index)
  }, [index, onDeleteLeafOption])

  const handleUpdateOptions = useCallback((nextLeafValue, childrenIndex) => {
    const { options, onUpdateOptions, index, childrenKey } = refProps.current
    const nextOptions = { ...options, [childrenKey]: [...options[childrenKey]] }
    nextOptions[childrenKey][childrenIndex] = nextLeafValue
    onUpdateOptions(nextOptions, index)
  }, [])

  const deleteLeaf = useCallback(childrenIndex => {
    const { options, onUpdateOptions, index, childrenKey } = refProps.current
    const nextOptions = { ...options, [childrenKey]: [...options[childrenKey]] }
    nextOptions[childrenKey].splice(childrenIndex, 1)
    onUpdateOptions(nextOptions, index)
  }, [])

  const selectNode = useCallback(
    () => onSelect({ node: options, sequence: handleGetSequence() }),
    [onSelect, options, handleGetSequence],
  )

  const onDragStart = useCallback(() => {
    const { setDropState } = refProps.current
    setDropState({ node: refProps.current, sequence: handleGetSequence() })
  }, [handleGetSequence])

  const OpenStateIcon = children ? (expanded ? CloseIcon : OpenIcon) : ChildrenLessIcon

  return (
    <LeafContainer level={level} selected={leafVal === selectedNode} className={className}>
      <HeaderContainer
        className="flex items-center bg-inherit"
        onDrop={onDrop}
        onDragOver={onDragOver}
        onDragLeave={onDragLeave}
      >
        <OpenStateIcon onClick={toggleOpen} className="mr-1.5" />
        {checkAble && (
          <CheckBox
            id={`${level}_${index}`}
            className="mr-1.5"
            onInput={checkBoxInput}
            value={getLeafSelectedStatus(options)}
          />
        )}
        <Row
          level={level}
          title={title}
          onClick={selectNode}
          selected={leafVal === selectedNode}
          borderState={borderState}
          draggable={draggable}
          onDragStart={onDragStart}
          onDragEnd={onDragEnd}
          node={options}
          parent={parent}
          rowComponent={rowComponent}
          onInput={onUpdateLeafOption}
          onDelete={handleDeleteLeafOption}
        />
      </HeaderContainer>
      <ChildrenContainer>
        {expanded &&
          children &&
          children?.map((item, index) => (
            <LeafComponent
              getSequence={handleGetSequence}
              LeafComponent={LeafComponent}
              dropRule={dropRule}
              draggable={draggable}
              key={item[valueKey]}
              options={item}
              checkAble={checkAble}
              index={index}
              level={level + 1}
              onInput={onInput}
              getLeafSelectedStatus={getLeafSelectedStatus}
              onSelect={onSelect}
              selectedNode={selectedNode}
              parent={options}
              defaultExpandAll={defaultExpandAll}
              setDropState={setDropState}
              dropState={dropState}
              rowComponent={rowComponent}
              onUpdateOptions={handleUpdateOptions}
              onDeleteLeafOption={deleteLeaf}
            />
          ))}
      </ChildrenContainer>
    </LeafContainer>
  )
}

Leaf.propTypes = {
  OpenIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  CloseIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  ChildrenLessIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  className: PropTypes.string,
  childrenKey: PropTypes.string.isRequired,
  valueKey: PropTypes.string.isRequired,
  labelKey: PropTypes.string,
  options: PropTypes.object.isRequired,
  level: PropTypes.number,
  onInput: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  defaultExpandAll: PropTypes.bool,
  getLeafSelectedStatus: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  selectedNode: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  draggable: PropTypes.bool,
  checkAble: PropTypes.bool,
  getSequence: PropTypes.func.isRequired,
  dropRule: PropTypes.func.isRequired,
  setDropState: PropTypes.func.isRequired,
  dropState: PropTypes.object.isRequired,
  rowComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  parent: PropTypes.object,
  onUpdateOptions: PropTypes.func.isRequired,
  onDeleteLeafOption: PropTypes.func.isRequired,
  LeafComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  returnObjects: PropTypes.bool,
}

Leaf.defaultProps = {
  className: '',
  childrenKey: 'children',
  valueKey: 'id',
  labelKey: 'title',
  OpenIcon: CirclePlusIcon,
  CloseIcon: CircleMinusIcon,
  ChildrenLessIcon: DotIcon,
  LeafComponent: Leaf,
  defaultExpandAll: false,
  draggable: false,
  checkAble: false,
  returnObjects: false,
  level: 0,
  selectedNode: '',
  rowComponent: undefined, // Определен в Row
  parent: undefined,
}

export default Leaf
