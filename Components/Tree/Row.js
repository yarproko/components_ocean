import PropTypes from 'prop-types'
import { LeafHeader, LeafTitle } from './styles'

const Row = ({
  title,
  onClick,
  selected,
  draggable,
  onDragStart,
  onDragEnd,
  borderState,
  node,
  rowComponent: Component,
  parent,
  onInput,
  onDelete,
  level,
}) => {
  return (
    <LeafHeader
      onClick={onClick}
      draggable={draggable}
      borderState={borderState}
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
    >
      <Component node={node} parent={parent} onInput={onInput} onDelete={onDelete} level={level}>
        <LeafTitle selected={selected}>{title}</LeafTitle>
      </Component>
    </LeafHeader>
  )
}

Row.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  draggable: PropTypes.bool,
  onDragStart: PropTypes.func.isRequired,
  onDragEnd: PropTypes.func.isRequired,
  borderState: PropTypes.string,
  node: PropTypes.object.isRequired,
  rowComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  parent: PropTypes.object,
  onInput: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  level: PropTypes.number.isRequired,
}

Row.defaultProps = {
  rowComponent: ({ children, className, style }) => (
    <div className={className} style={style}>
      {children}
    </div>
  ),
  selected: false,
  draggable: false,
  borderState: '',
  parent: undefined,
}

export default Row
