/*
  1. отделяем стейт вкладок от всего, храним урл и название вкладки
  2. стейт страницы выносим в recoil
  3. связующим звеном являеться tab_uiid
  4. все стейты страниц и детей являются плоскими, ключами их хэшей выступают Url+uiid вкладки
  5. есть общий реест стейтов вкладок, чтобы его отчищать при уничтожении стейта вкладки
  6. добавить энтити мап по ["type", ?"is"] для поддержки апдейта сущностей по сокетам
 */

import { useCallback, useContext, useEffect, useMemo, useRef } from 'react'
import { CurrentTabContext, TabStateManipulation } from './index'
import { useRecoilState } from 'recoil'
import { EntityMap, EntityPath, globalTabDataStorage, pagesQueue, TabStateDictionary } from './state'

const defaultPath = ({ stateId }) => [stateId]

const useTabItem = ({ stateId, setEntityPath = defaultPath }) => {
  const refPrevEntityPath = useRef() // теоретический может давать баг, в случаее вторжения в стейт извне и смены ключевых параметров, когда компонент размаунтен
  const { cleanUpEntitiesState } = useContext(TabStateManipulation)
  const {
    currentTabID,
    currentTabIndex,
    tabs: { [currentTabIndex]: { initialState } = {} },
  } = useContext(CurrentTabContext)
  const tabEntityStateId = useMemo(() => `${currentTabID}${stateId}`, [stateId, currentTabID])
  const [state, setState] = useRecoilState(globalTabDataStorage(tabEntityStateId))
  const { fetched, loading, error } = state
  const [queueState, setQueue] = useRecoilState(pagesQueue(currentTabID))
  const { stateRootCandidateId, stateRootId, lastClosedStateId } = queueState
  const refTabs = useRef(queueState)
  refTabs.current = queueState

  useEffect(() => {
    const { stateRootCandidateId, stateRootId, removeCandidatesStates } = refTabs.current
    if (stateRootCandidateId === stateId) {
      setQueue({
        ...refTabs.current,
        ...(stateRootId === undefined
          ? (() => {
              cleanUpEntitiesState(currentTabID, [...removeCandidatesStates])
              return {
                stateRootId: stateId,
                stateRootCandidateId: undefined,
                removeCandidatesStates: undefined,
              }
            })()
          : { stateRootCandidateId: undefined }),
      })
    }
  }, [lastClosedStateId, currentTabID])

  useEffect(() => {
    // записываем в словарь id вкладок ее стейты
    if (!TabStateDictionary[currentTabID]) {
      TabStateDictionary[currentTabID] = new Set()
    }
    TabStateDictionary[currentTabID].add(tabEntityStateId)
    // console.log(stateId, stateRootId, stateRootCandidateId)
    if (!stateRootId) {
      setQueue({ stateRootId: stateId })
    } else if (!stateRootCandidateId) {
      setQueue({ stateRootCandidateId: stateId })
    }

    // храним стейт id когда мы уходим из компонента
    return () => {
      const { stateRootId, removeCandidatesStates = new Set() } = refTabs.current
      removeCandidatesStates.add({ stateId, path: refPrevEntityPath.current })
      // console.log(6, stateRootId, stateId, {
      //   ...stateRootId === stateId ? {lastClosedStateId: stateId, stateRootId: undefined} : {},
      //   removeCandidatesStates: removeCandidatesStates,
      // })
      setQueue({
        ...(stateRootId === stateId ? { lastClosedStateId: stateId, stateRootId: undefined } : {}),
        removeCandidatesStates: removeCandidatesStates,
      })
    }
  }, [currentTabID])

  // TODO возможно стоит разбить на два хука. один высчитывает path. второй уже перепрописывает эти path
  useEffect(() => {
    const path = setEntityPath({ stateId, data: state.data })
    const pathAsString = path.join('/')

    if (pathAsString !== refPrevEntityPath.current) {
      // удаляем предидущему пути id вкладки
      if (refPrevEntityPath.current) {
        let obj = EntityMap
        refPrevEntityPath.current.split('/').forEach(p => {
          obj = obj[p]
        })
        new Set()
        obj.delete(currentTabID)

        // удаляем путь из id вкладки, если он есть
        if (EntityPath[currentTabID]) {
          EntityPath[currentTabID].delete(refPrevEntityPath.current)
        }
      }
      // Добавляем по номову пути id вкладки

      let obj = EntityMap
      path.forEach((p, index) => {
        if (!obj[p]) {
          obj[p] = index === path.length - 1 ? new Set() : {} // наполняем мапу в случаее отсутсвия дерева
        }
        obj = obj[p]
      })
      obj.add(currentTabID)

      // Добавляем по id вкладке путь, по которому мы будем удалять id вкладки из словаря сущностей
      if (!EntityPath[currentTabID]) {
        EntityPath[currentTabID] = new Set()
      }
      EntityPath[currentTabID].add(pathAsString)
    }
    // сохраняем путь, чтобы в случаее его смены сделать отчистку
    refPrevEntityPath.current = pathAsString
  }, [setEntityPath, state.data, currentTabID])

  return {
    initialState,
    tabState: state,
    setTabState: setState,
    shouldReloadDataFlag: useMemo(() => !fetched && !loading && !error, [currentTabID, fetched, loading, error]),
    loadDataHelper: useCallback(
      loadPageData => async () => {
        try {
          setState({ loading: true })
          setState({
            data: await loadPageData(),
            loading: false,
            error: false,
            loadedOnce: true,
            fetched: true,
          })
        } catch (e) {
          console.log(e)
          setState({ loading: false, error: true, fetched: false })
        }
      },
      [setState],
    ),
  }
}
export default useTabItem
