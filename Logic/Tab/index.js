import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { useRecoilState } from 'recoil'
import {
  EntityMap,
  EntityPath,
  // globalTabDataStorage,
  tabDataStorage,
  TabStateDictionary,
  tabStateSelector,
} from './state'
import { useLocation, useNavigate } from 'react-router-dom'

export const TabStateManipulation = React.createContext({})
export const CurrentTabContext = React.createContext({})
export const CACHED_TAB_STATE = 'CACHED_TAB_STATE'
export const LAST_ACTIVE_INDEX = 'LAST_ACTIVE_INDEX'

let tabId = 1

class Tab extends Component {
  constructor(props) {
    super(props)
    const {
      userId,
      location: { pathname, search },
    } = props
    const loc = `${pathname}${search}`
    // сетаем стейт для текущего юзера, если у нас нет юзера мы не можем быть на этой странице, замена юзера происходит
    // только через логин
    const state = {}
    const cachedTabState = localStorage.getItem(`${userId}${CACHED_TAB_STATE}`)
    const cachedLastTabIndex = localStorage.getItem(`${userId}${LAST_ACTIVE_INDEX}`)
    state.tabs = cachedTabState
      ? JSON.parse(cachedTabState).reduce((acc, state, index) => {
          acc[index] = { ...state, id: tabId + index }
          return acc
        }, [])
      : []
    state.currentTabIndex = cachedLastTabIndex !== null ? Number(cachedLastTabIndex) : 0

    if (state.tabs.length > 0 && state.tabs.every(t => t.pathname !== loc)) {
      state.tabs.unshift({
        id: tabId + state.tabs.length,
        pathname: loc,
      })
      state.currentTabIndex = 0
    }

    tabId = state.tabs.length + tabId

    state.currentTabID = state.tabs[state.currentTabIndex]?.id || 1
    props.setTabState(state)

    this.manipulateFunctions = {
      updateCurrentTab: this.updateCurrentTab,
      onCloseTab: this.onCloseTab,
      openNewTab: this.onOpenNewTab,
      changeActiveTab: this.changeActiveTab,
      cleanUpEntitiesState: this.cleanUpEntitiesState,
    }
  }

  shouldComponentUpdate({
    navigation,
    location: { pathname, search },
    tabState: {
      currentTabIndex,
      tabs: { [currentTabIndex]: { pathname: nextTabPathName } = {} },
    },
  }) {
    const {
      location: { pathname: prevPathName, search: prevSearch },
    } = this.props
    const loc = `${pathname}${search}`
    const prevLoc = `${prevPathName}${prevSearch}`

    if (prevLoc !== loc && loc !== nextTabPathName) {
      this.updateCurrentTab({ pathname: loc })
      return false
    }
    if (nextTabPathName && nextTabPathName !== loc) {
      navigation(nextTabPathName)
      return false
    }
    return true
  }

  updateCurrentTab = state => {
    const { setTabState } = this.props
    setTabState(({ tabs, currentTabIndex, ...prevState }) => {
      const nextTabs = [...tabs]
      nextTabs.splice(currentTabIndex, 1, {
        ...nextTabs[currentTabIndex],
        ...state,
      })
      const nextState = {
        tabs: nextTabs,
        currentTabIndex,
        ...prevState,
      }
      this.storeTabsInLocalStorage(nextState)
      return nextState
    })
  }

  storeTabsInLocalStorage = ({ tabs, currentTabIndex }) => {
    const {
      props: { userId },
    } = this
    localStorage.setItem(`${userId}${CACHED_TAB_STATE}`, JSON.stringify(tabs))
    localStorage.setItem(`${userId}${LAST_ACTIVE_INDEX}`, currentTabIndex)
  }

  onCloseTab = index => {
    const {
      props: { setTabState, setTabData, tabData },
    } = this
    const nextTabData = { ...tabData }
    setTabState(({ tabs, currentTabIndex }) => {
      const nextTabs = [...tabs]
      const [{ id }] = nextTabs.splice(index, 1)
      const nextTabIndex =
        index < currentTabIndex // если закрываем вкладку перед текущей вкладку
          ? currentTabIndex - 1 // уменьшаем текущий индекс на -1(одна закрытая вкладка)
          : index === currentTabIndex // если закрываем текущую вкладку
          ? nextTabs[index - 1] // пытаемся открыть предидущую или оставляем следующую, которая встанет на место текущей
            ? index - 1
            : index
          : currentTabIndex // оставляем активный индекс неизменным
      const nextState = {
        tabs: nextTabs,
        currentTabIndex: nextTabIndex,
        currentTabID: nextTabs[nextTabIndex].id,
      }
      this.storeTabsInLocalStorage(nextState)
      const { [id]: states = [] } = TabStateDictionary

      states.forEach(stateId => {
        delete nextTabData[stateId]
      })

      delete TabStateDictionary[id]

      const { [id]: paths = [] } = EntityPath
      paths.forEach(path => {
        let obj = EntityMap
        path.split('/').forEach(p => {
          obj = obj[p]
        })
        obj.delete(tabId)
      })
      delete EntityPath[id]
      return nextState
    })

    setTabData(nextTabData)
  }

  onChangeActiveTab = currentTabIndex => {
    const {
      props: { setTabState },
    } = this
    setTabState(({ tabs }) => {
      const nextState = {
        tabs: tabs,
        currentTabIndex: currentTabIndex,
        currentTabID: tabs[currentTabIndex].id,
      }
      this.storeTabsInLocalStorage(nextState)
      return nextState
    })
  }

  onOpenNewTab = (pathname, state) => {
    const {
      props: { setTabState },
    } = this
    tabId = tabId + 1
    setTabState(({ tabs }) => {
      const id = tabId
      const nextState = {
        tabs: [...tabs, { id, pathname: encodeURI(pathname), initialState: state }],
        currentTabIndex: tabs.length,
        currentTabID: id,
      }
      this.storeTabsInLocalStorage(nextState)
      return nextState
    })
  }

  cleanUpEntitiesState = (tabId, removedStates) => {
    const { tabData, setTabData } = this.props
    const nextTabData = { ...tabData }
    removedStates.forEach(({ stateId, path }) => {
      const tabsStateId = `${tabId}${stateId}`
      if (TabStateDictionary[tabId]) {
        TabStateDictionary[tabId].delete(tabsStateId)
      }
      delete nextTabData[tabsStateId]
      let obj = EntityMap
      path.split('/').forEach(p => {
        obj = obj[p]
      })
      obj.delete(tabId)
      if (EntityPath[tabId]) {
        EntityPath[tabId].delete(path)
      }
    })
    setTabData(nextTabData)
  }

  componentWillUnmount() {
    const { setTabData } = this.props
    // сбрасываем стейт влкадок и все словари для поиска стейта из вкладок
    setTabData({})
    return [TabStateDictionary, EntityMap, EntityPath].forEach(dictionary => {
      const keys = Object.keys(dictionary)
      keys.forEach(key => {
        delete dictionary[key]
      })
    })
  }

  render() {
    const {
      props: { children, tabState },
    } = this

    return (
      <TabStateManipulation.Provider value={this.manipulateFunctions}>
        <CurrentTabContext.Provider value={tabState}>
          {tabState.tabs &&
            children({
              tabState,
              onOpenNewTab: this.onOpenNewTab,
              onCloseTab: this.onCloseTab,
              onChangeActiveTab: this.onChangeActiveTab,
            })}
        </CurrentTabContext.Provider>
      </TabStateManipulation.Provider>
    )
  }
}

Tab.propTypes = {
  children: PropTypes.func.isRequired,
  userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  location: PropTypes.object.isRequired,
  navigation: PropTypes.func.isRequired,
  tabState: PropTypes.shape({
    currentTabID: PropTypes.number.isRequired,
    currentTabIndex: PropTypes.number.isRequired,
    tabs: PropTypes.array.isRequired,
  }).isRequired,
  setTabState: PropTypes.func.isRequired,
  setTabData: PropTypes.func.isRequired,
  tabData: PropTypes.object.isRequired,
}

const stateSelector = Comp => {
  const TabStateSelector = ({ userId, ...props }) => {
    const navigation = useNavigate()
    const location = useLocation()
    const [tabState, setTabState] = useRecoilState(tabStateSelector(userId))
    const [tabData, setTabData] = useRecoilState(tabDataStorage)

    return (
      <Comp
        {...props}
        userId={userId}
        tabState={tabState}
        setTabState={setTabState}
        navigation={navigation}
        location={location}
        tabData={tabData}
        setTabData={setTabData}
      />
    )
  }
  TabStateSelector.propTypes = {
    userId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }

  return TabStateSelector
}

export default stateSelector(Tab)
