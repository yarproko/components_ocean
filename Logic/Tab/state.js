import { atom, atomFamily, selectorFamily } from 'recoil'

export const tabCachedState = atomFamily({
  key: 'tabCachedState',
  default: () => ({}),
})

export const tabStateSelector = selectorFamily({
  key: 'tabState',
  get:
    userId =>
    ({ get }) =>
      get(tabCachedState(userId)),
  set:
    userId =>
    ({ set }, newValue) => {
      set(tabCachedState(userId), newValue)
    },
})

export const tabDataStorage = atom({
  key: 'tabDataStorage',
  default: {},
})
export const globalTabDataStorage = selectorFamily({
  key: 'globalTabDataStorage',
  get:
    stateId =>
    ({ get }) =>
      get(tabDataStorage)[stateId] || {},
  set:
    stateId =>
    ({ set, get }, newValue) => {
      // localStorage.setItem(generateUserLocalStorageKey(userId), JSON.stringify(newValue))
      const tabDataState = get(tabDataStorage)
      set(tabDataStorage, { ...tabDataState, [stateId]: { ...tabDataState[stateId], ...newValue } })
    },
})

export const pagesQueueAtom = atomFamily({
  key: 'pagesQueueAtom',
  default: () => ({}),
})

export const pagesQueue = selectorFamily({
  key: 'pagesQueue',
  get:
    tabId =>
    ({ get }) =>
      get(pagesQueueAtom(tabId)),
  set:
    tabId =>
    ({ set, get }, newValue) => {
      return set(pagesQueueAtom(tabId), { ...get(pagesQueueAtom(tabId)), ...newValue })
    },
})

// объект с реестром данных вкладки, у вкладки могут быть неограниченное кол-во подвкладок,
// на нужно отчищать неактуальные стейты, поэтому при закрытии вкладок мы будем смотреть сюда и удалять связанные стейты
export const TabStateDictionary = {} // { "12": ["LIST_ITEM", "LIST_ITEM_REQUISITES"] }
// объект с ключами энтитей, внутри лежат объекты с массивами
export const EntityMap = {} // примрер { LIST_ITEM: { 12314: Set("12") } }
// объект для обратного маппинга, нужен, чтобы удалить пути до не актуальных энтитей
export const EntityPath = {} // пример( { "12": Set(["LIST_ITEM", "12314"]) }
//
// export const userMessages = selectorFamily({
//   key: "userMessages",
//   get: (params) => ({ get }) => getChatState(params, get(chatMessages)),
//   set: (params) => ({ get, set }, nextState) => {
//     set(chatMessages, updateMessages(params)(get(chatMessages), nextState))
//   }
// })
