import { useEffect, useRef } from 'react'

const useAutoReload = ({ loadDataFunction, shouldReloadDataFlag }) => {
  // закидываем в рефу функцию для первого рендера. тогда мы гарантируем первую загрузку по флагу.
  // кейс повторного открытия компонента с загруженой датой
  // в случае смены функции - перезагружаем дату для компонента
  const refLoadDataFunction = useRef(loadDataFunction)

  useEffect(() => {
    if (shouldReloadDataFlag || loadDataFunction !== refLoadDataFunction.current) {
      loadDataFunction()
    }
    refLoadDataFunction.current = loadDataFunction
  }, [loadDataFunction, shouldReloadDataFlag])
}

export default useAutoReload
