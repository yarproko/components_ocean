import { useContext, useEffect } from 'react'
import { CurrentTabContext, TabStateManipulation } from './index'

const useSetTabName = setTabName => {
  const { updateCurrentTab } = useContext(TabStateManipulation)
  const { currentTabID } = useContext(CurrentTabContext)
  useEffect(() => {
    updateCurrentTab({ name: setTabName() })
  }, [setTabName, updateCurrentTab, currentTabID])
}

export default useSetTabName
