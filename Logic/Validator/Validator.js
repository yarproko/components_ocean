import { set } from '../../Utils/ObjectPath'
import memoize from 'lodash/memoize'
import rules from './rules'

export default class Validator {
  mergeValidators = memoize(rules => ({ ...Validator.defaultRules, ...rules }))

  parseRule = memoize((rulesObjs, validators) =>
    Object.entries(rulesObjs).reduce(
      (acc, [path, rules], index) => {
        const { r, i } = rules.reduce(
          (a, rule) => {
            const r = validators[rule.name]
            if (r.implicit) {
              a.i.push({ ...r, args: rule.args, path })
            } else {
              a.r.push(rule)
            }
            return a
          },
          { r: [], i: [] },
        )

        if (i.length > 0) {
          acc.implicit.push([index, i])
          acc.hasImplicit = true
        }
        if (r.length > 0) {
          acc.rules.push([path.split('.'), r])
        }

        return acc
      },
      { rules: [], implicit: [], hasImplicit: false },
    ),
  )

  addImplicitRules = memoize((formPayload, { rules, implicit, hasImplicit }) => {
    return !hasImplicit
      ? rules
      : implicit.reduce(
          (acc, [index, i]) => {
            i.forEach(({ implicit, args, rule, path }) => {
              if (implicit({ formPayload, args })) {
                const { [index]: [p, rules] = [path, []] } = acc
                acc[index] = [p, [...rules, { name: rule, args }]]
              }
            })

            return acc
          },
          [...rules],
        )
  })

  static defaultRules = rules

  validateInput = validators => {
    const validate = (input, path, rules, errors) => {
      let prevTempValue = input
      let tempValue = input

      for (let pathIndex = 0; pathIndex < path.length; pathIndex++) {
        const key = path[pathIndex]
        if (key === '*') {
          const nextErrors = {}
          const restPath = path.slice(pathIndex + 1)
          if (tempValue) {
            // eslint-disable-next-line no-loop-func
            tempValue.forEach((elem, index) => {
              validate(tempValue, [index, ...restPath], rules, nextErrors)
            })
          }
          if (Object.keys(nextErrors).length > 0) {
            set(path.slice(0, pathIndex), errors, nextErrors)
          }
          return errors
        }
        prevTempValue = tempValue
        tempValue = tempValue ? tempValue[key] : tempValue
      }
      const fieldErrors = rules.reduce((fieldErrors, { name, args }) => {
        const { resolver, message, nullAble } = validators[name]
        if (tempValue !== undefined || tempValue !== '' || nullAble) {
          const resolverArgs = {
            value: tempValue,
            args,
            formPayload: prevTempValue,
            totalValue: input,
            path,
            validators,
          }
          if (!resolver(resolverArgs)) {
            fieldErrors.push(typeof message === 'function' ? message(resolverArgs) : message)
          }
        }
        return fieldErrors
      }, [])
      if (fieldErrors.length > 0) {
        set(path, errors, fieldErrors)
      }
      return errors
    }
    return validate
  }

  validate = (input, rules, validators) => {
    const errors = {}
    const mergedValidators = this.mergeValidators(validators)
    const validator = this.validateInput(mergedValidators)
    const parsedRules = this.parseRule(rules, mergedValidators)
    this.addImplicitRules(input, parsedRules).forEach(fieldRule => {
      validator(input, ...fieldRule, errors)
    })
    return errors
  }
}
