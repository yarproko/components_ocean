import { Component } from 'react'
import PropTypes from 'prop-types'
import Validator from './Validator'

const WithValidationHoc = OriginalComponent => {
  class WithValidation extends Component {
    constructor(props) {
      super(props)
      this.state = {
        validator: new Validator(),
        prevValue: undefined,
        validationErrors: {},
        touched: {},
        changed: {},
        formValid: undefined,
        submitFailed: false,
        formHasSubmitted: false,
      }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
      const { value, rules, validators } = nextProps
      if (value !== prevState.prevValue) {
        const errors = prevState.validator.validate(value, rules, validators)
        return {
          formValid: Object.keys(errors).length === 0,
          validationErrors: errors || {},
          prevValue: value,
        }
      }
      return null
    }

    componentDidMount() {
      const { value } = this.props
      this.validate(value)
    }

    validate = values => {
      const {
        props: { rules, validators },
        state: { validator },
      } = this
      const errors = validator.validate(values, rules, validators)
      if (Object.keys(errors).length > 0) {
        this.setState({
          formValid: false,
          validationErrors: errors,
        })
        return new Error('error was happened during validation evaluation')
      }
      this.setState({
        formValid: true,
        validationErrors: {},
      })
      return values
    }

    handleSubmit = async () => {
      try {
        const {
          props: { value, onSubmit },
        } = this
        const result = this.validate(value)
        if (result instanceof Error) {
          const { failFormSubmitStatus = 416 } = this
          throw { response: { status: failFormSubmitStatus } }
        }
        // должна быть промисом
        const data = await onSubmit(result)
        this.resetForm()
        this.setState({ formHasSubmitted: true })
        return data
      } catch (e) {
        // eslint-disable-next-line no-unused-vars
        const { response: { status, data = {} } = {} } = e
        this.setFormSubmitFlag()
        // TODO доплить хэндл валидации, на момент написания кода бэк не опеределился с ответом
        // this.validationErrors = parseValidationErros(data);
        return e
      }
    }

    handleBlur = id => () => {
      this.setState(state => ({ changed: { ...state.changed, [id]: true } }))
    }

    handleFocus = id => () => {
      this.setState(state => ({ touched: { ...state.touched, [id]: true } }))
    }

    setFormSubmitFlag = () => {
      this.setState({
        submitFailed: true,
        formHasSubmitted: true,
      })
    }

    handleInput = newFormValue => {
      this.state.prevValue = newFormValue
      this.validate(newFormValue)
      this.props.onInput(newFormValue)
    }

    resetForm = () => {
      this.setState({
        validationErrors: {},
        submitFailed: false,
        formHasSubmitted: false,
        formPayload: {},
        touched: {},
        changed: {},
      })
    }

    render() {
      const {
        handleBlur,
        handleFocus,
        handleSubmit,
        handleInput,
        validate,
        props: { onSubmit },
      } = this
      return (
        <OriginalComponent
          {...this.props}
          {...this.state}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onSubmit={onSubmit ? handleSubmit : undefined}
          onInput={handleInput}
          validateForm={validate}
        />
      )
    }
  }

  WithValidation.propTypes = {
    onInput: PropTypes.func.isRequired,
    onSubmit: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    rules: PropTypes.object,
    validators: PropTypes.object,
  }

  WithValidation.defaultProps = {
    value: {},
    rules: {},
    onSubmit: () => null,
    validators: {},
  }
  return WithValidation
}

export default WithValidationHoc

export const Validation = WithValidationHoc(({ children, ...props }) => children(props))
