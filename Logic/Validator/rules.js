import dayjs from 'dayjs'

import {
  VALIDATION_RULE_ACCEPTED,
  VALIDATION_RULE_BIG_DATE_THEN,
  VALIDATION_RULE_DATE,
  VALIDATION_RULE_DATE_AFTER,
  VALIDATION_RULE_DATE_AFTER_OR_EQUAL,
  VALIDATION_RULE_DATE_BEFORE,
  VALIDATION_RULE_DATE_BEFORE_OR_EQUAL,
  VALIDATION_RULE_DIGITS_AND_INTEGER,
  VALIDATION_RULE_EMAIL,
  VALIDATION_RULE_INTEGER,
  VALIDATION_RULE_LESS_DATE_THEN,
  VALIDATION_RULE_MAX,
  VALIDATION_RULE_MIN,
  VALIDATION_RULE_NULL_IF,
  VALIDATION_RULE_PASSWORD,
  VALIDATION_RULE_PHONE,
  VALIDATION_RULE_REGEX,
  VALIDATION_RULE_REQUIRED,
  VALIDATION_RULE_REQUIRED_IF,
  VALIDATION_RULE_REQUIRED_WITH_ALL,
  VALIDATION_RULE_REQUIRED_WITHOUT,
  VALIDATION_RULE_REQUIRED_WITHOUT_ALL,
  VALIDATION_RULE_SAME,
  VALIDATION_RULE_SIZE,
} from './constants'

const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})')
const acceptedSet = new Set(['on', 'yes', 1, '1', true])
// eslint-disable-next-line
const mailRegex = /(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
const emptyObj = {}
const getSize = value => (typeof value === 'number' ? parseFloat(value, 10) : value.length)

export default {
  [VALIDATION_RULE_BIG_DATE_THEN]: {
    resolver({ value, args: { fieldKey } = emptyObj, formPayload: { [fieldKey]: sameField } }) {
      return dayjs(value, 'DD.MM.YYYY').valueOf() >= dayjs(sameField, 'DD.MM.YYYY').valueOf()
    },
    message: 'End date must bee bigger than start date',
  },
  [VALIDATION_RULE_LESS_DATE_THEN]: {
    resolver({ value, args: { fieldKey } = emptyObj, formPayload: { [fieldKey]: sameField } }) {
      return dayjs(value, 'DD.MM.YYYY').valueOf() <= dayjs(sameField, 'DD.MM.YYYY').valueOf()
    },
    message: 'Start date must bee less than end date',
  },
  [VALIDATION_RULE_SAME]: {
    resolver: ({ value, args: { fieldKey } = emptyObj, formPayload: { [fieldKey]: sameField } }) => value === sameField,
    message: ({ args: { fieldKey, fieldKeyLabel = fieldKey } }) => `Значение поля ${fieldKeyLabel} должно совпадать.`,
  },
  [VALIDATION_RULE_PASSWORD]: {
    resolver: ({ value }) => strongRegex.test(value),
    message: 'Пароль должен содержать хотя бы 1 заглавную букву, символ и цифру',
  },
  [VALIDATION_RULE_REQUIRED]: {
    resolver: ({ value }) => {
      if (typeof value === 'string') {
        return String(value).replace(/\s/g, '').length > 0
      }
      if (Array.isArray(value)) {
        return value.length > 0
      }
      return !(value === undefined || value === null)
    },
    nullAble: true,
    message: 'Поле обязательно к заполнению',
  },
  [VALIDATION_RULE_ACCEPTED]: {
    resolver: ({ value }) => acceptedSet.has(value),
    message: 'Значение поля должно быть одним из: on, yes, "1", 1, true',
  },
  [VALIDATION_RULE_SIZE]: {
    resolver: ({ value, args: { size } = emptyObj }) => getSize(value) - size >= 0,
    message: ({ args: { size } = emptyObj }) => `Количество символов дожно быть не менее ${size}`,
  },
  [VALIDATION_RULE_DIGITS_AND_INTEGER]: {
    resolver: ({ value, args: { digits } = emptyObj }) =>
      typeof value === 'number' &&
      !isNaN(value) &&
      parseInt(value, 10) === value &&
      digits - String(value).length === 0,
    message: ({ value, args: { digits } }) =>
      digits - String(value).length === 0
        ? 'Значение поля должно быть целым числом'
        : `Количество символов дожно быть не более ${digits}`,
  },
  [VALIDATION_RULE_INTEGER]: {
    resolver: ({ value }) => parseInt(value, 10) === value,
    message: 'Значение поля должно быть целым числом',
  },
  [VALIDATION_RULE_MIN]: {
    resolver: ({ value, args: { min } }) => typeof value === "string" && value.length - min >= 0,
    message: ({ args: {min} }) => `Количество символов дожно быть не менее ${min}`
  },
  [VALIDATION_RULE_MAX]: {
    resolver: ({ value, args: { max } = emptyObj }) => typeof value === 'string' && max - value.length >= 0,
    message: ({ args: { max } = emptyObj }) => `Количество символов дожно быть не более ${max}`,
  },
  [VALIDATION_RULE_NULL_IF]: {
    resolver: ({ value, args: { fieldKey, fieldValue } = emptyObj, formPayload: { [fieldKey]: targetField } }) =>
      targetField !== fieldValue || !value,
    message: 'Значение поля должно быть пустым',
  },
  [VALIDATION_RULE_REGEX]: {
    resolver: ({ value, args: { regex } = emptyObj }) => regex.test(value),
    message: 'Значение поля не соотвествует формату',
  },
  [VALIDATION_RULE_DATE]: {
    resolver: ({ value, args: { date = 'DD.MM.YYYY' } = emptyObj }) => dayjs(value, date).isValid(),
    message: 'Значение поля не соотвествует формату',
  },
  [VALIDATION_RULE_DATE_BEFORE]: {
    resolver: ({ value, args: { format = 'DD.MM.YYYY', before = dayjs().format(format) } = emptyObj }) =>
      dayjs(value, format).valueOf() - dayjs(before, format).valueOf() < 0,
    message: ({ args: { format = 'DD.MM.YYYY', before = dayjs().format(format) } }) => `Дата должна быть до ${before}`,
  },
  [VALIDATION_RULE_DATE_BEFORE_OR_EQUAL]: {
    resolver: ({ value, args: { format = 'DD.MM.YYYY', before_or_equal = dayjs().format(format) } = emptyObj }) =>
      dayjs(value, format).valueOf() - dayjs(before_or_equal, format).valueOf() <= 0,
    message: ({ args: { format = 'DD.MM.YYYY', before_or_equal = dayjs().format(format) } }) =>
      `Дата должна быть до или равной ${before_or_equal}`,
  },
  [VALIDATION_RULE_DATE_AFTER]: {
    resolver: ({ value, args: { format = 'DD.MM.YYYY', after = dayjs().format(format) } = emptyObj }) =>
      dayjs(value, format).valueOf() - dayjs(after, format).valueOf() > 0,
    message: ({ args: { format = 'DD.MM.YYYY', after = dayjs().format(format) } }) => `Дата должна быть после ${after}`,
  },
  [VALIDATION_RULE_DATE_AFTER_OR_EQUAL]: {
    resolver: ({ value, args: { format = 'DD.MM.YYYY', after_or_equal = dayjs().format(format) } = emptyObj }) =>
      dayjs(value, format).valueOf() - dayjs(after_or_equal, format).valueOf() >= 0,
    message: ({ args: { format = 'DD.MM.YYYY', after_or_equal = dayjs().format(format) } = emptyObj }) =>
      `Дата должна быть после или равной ${after_or_equal}`,
  },
  [VALIDATION_RULE_REQUIRED_IF]: {
    implicit: ({ args: { fieldKey, fieldValue } = emptyObj, formPayload: { [fieldKey]: targetField } }) =>
      targetField === fieldValue,
    rule: VALIDATION_RULE_REQUIRED,
  },
  [VALIDATION_RULE_REQUIRED_WITH_ALL]: {
    implicit: ({ args: { fieldsKeys } = emptyObj, formPayload }) => fieldsKeys.every(key => !!formPayload[key]),
    rule: VALIDATION_RULE_REQUIRED,
  },
  [VALIDATION_RULE_REQUIRED_WITHOUT_ALL]: {
    implicit: ({ args: { fieldsKeys } = emptyObj, formPayload }) => fieldsKeys.every(key => !formPayload[key]),
    rule: VALIDATION_RULE_REQUIRED,
  },
  [VALIDATION_RULE_REQUIRED_WITHOUT]: {
    implicit: ({ args: { fieldsKeys } = emptyObj, formPayload }) => fieldsKeys.some(key => !formPayload[key]),
    rule: VALIDATION_RULE_REQUIRED,
  },
  [VALIDATION_RULE_PHONE]: {
    resolver: ({ args: { regex = /^\+(?:[0-9]●?){6,14}[0-9]$/ } = emptyObj, value }) => regex.test(value),
    message: 'Введите корректный номер телефона',
  },
  [VALIDATION_RULE_EMAIL]: {
    resolver: ({ args: { regex = mailRegex } = emptyObj, value }) => regex.test(value),
    message: 'Введите корректный email',
  },
}
