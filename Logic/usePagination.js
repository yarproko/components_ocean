import { useCallback, useMemo } from 'react'

const LOCAL_STORAGE_PAGINATION_PLUGIN_KEY = 'LOCAL_STORAGE_PAGINATION_PLUGIN_KEY'

const usePagination = ({ state: { page = 1, limit }, setState, stateId, defaultLimit = 10 }) => {
  return {
    setLimit: useCallback(
      limit => {
        localStorage.setItem(`${LOCAL_STORAGE_PAGINATION_PLUGIN_KEY}${stateId}`, limit)
        setState({ limit })
      },
      [setState, stateId],
    ),
    setPage: useCallback(
      page => {
        setState({ page })
      },
      [setState],
    ),
    paginationState: useMemo(() => {
      const l = Number(
        limit || localStorage.getItem(`${LOCAL_STORAGE_PAGINATION_PLUGIN_KEY}${stateId}`) || defaultLimit,
      )
      const offset = (page - 1) * l
      return {
        page,
        limit: l,
        offset,
        startItemValue: offset + 1,
        endItemValue: offset + l,
      }
    }, [defaultLimit, limit, page, stateId]),
  }
}

export default usePagination
