export default (num = 0) => Number(Number(num.toFixed(8)).toPrecision(16))
