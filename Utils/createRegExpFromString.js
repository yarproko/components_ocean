const createRegExpFromString = string => {
  const [expr, flags] = string.match(/(?!\/).*[^gmsyuid/]+|[gmsyuid]{0,7}$/gm)
  return new RegExp(expr, flags)
}

export default createRegExpFromString
