import React from 'react'
export const ContainerContext = React.createContext(document.body)
export const ASC = 'asc'
export const DSC = 'desc'
